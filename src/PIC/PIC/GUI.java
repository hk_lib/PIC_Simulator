package PIC;

import javax.swing.*;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.table.DefaultTableModel;
import javax.swing.text.DefaultHighlighter;
import javax.swing.text.Highlighter;
import javax.swing.text.Highlighter.HighlightPainter;

import java.awt.Color;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Hashtable;

public class GUI extends JFrame {
    private JMenuBar menuBar;

    private JMenu menuFile;
    private JMenuItem menuItemFileOpenFile;
    private JFileChooser fileChooserFileControlProgrammName;
    private String stringProgrammName;
    private JMenuItem menuItemFileCloseFile;

    private JMenu menuOther;
    private JMenuItem menuItemOtherHelp;
    private JOptionPane optionPaneOtherHelp;
    private JMenuItem menuItemOtherInfo;
    private JOptionPane optionPaneOtherInfo;


    private JPanel panelOne;

    private JPanel panelOneOne;

    private JPanel panelProgram;
    private JTextArea textAreaProgram;
    private JScrollPane scrollPaneProgram;
    private Highlighter hightlighterProgram;
    private HighlightPainter hightlightPainterProgram;

    private JPanel panelOneTwo;

    private JPanel panelRegisterMemory;
    private DefaultTableModel defaultTableModelRegisterMemory;
    private JTable tableRegisterMemory;
    private JScrollPane scrollPaneRegisterMemory;


    private JPanel panelTwo;

    private JPanel panelTwoOne;

    private JPanel panelOther;
    private JLabel labelOtherProgrammCounter;
    private JLabel labelOtherProgrammCounterValue;
    private JLabel labelOtherWRegister;
    private JLabel labelOtherWRegisterValue;

    private JPanel panelTwoTwo;

    private JPanel panelRegisterStatus;
    private JLabel labelRegisterStatusC;
    private JLabel labelRegisterStatusCValue;
    private JLabel labelRegisterStatusDC;
    private JLabel labelRegisterStatusDCValue;
    private JLabel labelRegisterStatusZ;
    private JLabel labelRegisterStatusZValue;
    private JLabel labelRegisterStatusPD;
    private JLabel labelRegisterStatusPDValue;
    private JLabel labelRegisterStatusTO;
    private JLabel labelRegisterStatusTOValue;
    private JLabel labelRegisterStatusRP0;
    private JLabel labelRegisterStatusRP0Value;
    private JLabel labelRegisterStatusRP1;
    private JLabel labelRegisterStatusRP1Value;
    private JLabel labelRegisterStatusIRP;
    private JLabel labelRegisterStatusIRPValue;

    private JPanel panelTwoThree;

    private JPanel panelEightLevelStack;
    private JLabel labelEightLevelStackZero;
    private JLabel labelEightLevelStackZeroValue;
    private JLabel labelEightLevelStackOne;
    private JLabel labelEightLevelStackOneValue;
    private JLabel labelEightLevelStackTwo;
    private JLabel labelEightLevelStackTwoValue;
    private JLabel labelEightLevelStackThree;
    private JLabel labelEightLevelStackThreeValue;
    private JLabel labelEightLevelStackFour;
    private JLabel labelEightLevelStackFourValue;
    private JLabel labelEightLevelStackFive;
    private JLabel labelEightLevelStackFiveValue;
    private JLabel labelEightLevelStackSix;
    private JLabel labelEightLevelStackSixValue;
    private JLabel labelEightLevelStackSeven;
    private JLabel labelEightLevelStackSevenValue;

    private JPanel panelTwoFour;

    private JPanel panelPinOut;

    private JPanel panelPinOutOne;
    private JLabel labelPinOutOneOSC1CLKIN;
    private JButton buttonPinOutOneOSC1CLKINValue;
    private JLabel labelPinOutOneOSC2CLKOUT;
    private JButton buttonPinOutOneOSC2CLKOUTValue;
    private JLabel labelPinOutOneMCLR;
    private JButton buttonPinOutOneMCLRValue;
    private JLabel labelPinOutOneRA0;
    private JButton buttonPinOutOneRA0Value;
    private JLabel labelPinOutOneRA1;
    private JButton buttonPinOutOneRA1Value;
    private JLabel labelPinOutOneRA2;
    private JButton buttonPinOutOneRA2Value;
    private JLabel labelPinOutOneRA3;
    private JButton buttonPinOutOneRA3Value;
    private JLabel labelPinOutOneRA4T0CKI;
    private JButton buttonPinOutOneRA4T0CKIValue;
    private JLabel labelPinOutOneVSS;
    private JButton buttonPinOutOneVSSValue;

    private JPanel panelPinOutTwo;
    private JLabel labelPinOutTwoRB0INT;
    private JButton buttonPinOutTwoRB0INTValue;
    private JLabel labelPinOutTwoRB1;
    private JButton buttonPinOutTwoRB1Value;
    private JLabel labelPinOutTwoRB2;
    private JButton buttonPinOutTwoRB2Value;
    private JLabel labelPinOutTwoRB3;
    private JButton buttonPinOutTwoRB3Value;
    private JLabel labelPinOutTwoRB4;
    private JButton buttonPinOutTwoRB4Value;
    private JLabel labelPinOutTwoRB5;
    private JButton buttonPinOutTwoRB5Value;
    private JLabel labelPinOutTwoRB6;
    private JButton buttonPinOutTwoRB6Value;
    private JLabel labelPinOutTwoRB7;
    private JButton buttonPinOutTwoRB7Value;
    private JLabel labelPinOutTwoVDD;
    private JButton buttonPinOutTwoVDDValue;

    private JPanel panelTwoFive;

    private JPanel panelTiming;

    private JPanel panelTimingOne;
    private JLabel labelTimingOneRunTime;
    private JLabel labelTimingOneRunTimeValue;
    private JLabel labelTimingOneTMR0;
    private JLabel labelTimingOneTMR0Value;
    private JLabel labelTimingOneWDT;
    private JLabel labelTimingOneWDTValue;
    private JLabel labelTimingOneFrequency;
    private JLabel labelTimingOneFrequencyValue;
    private JPanel panelTimingTwo;
    private JSlider sliderTimingTwoFrequency;
    private Hashtable<Integer, JLabel> hashtableTimingTwoFrequency;

    private JPanel panelTwoSix;

    private JPanel panelControl;

    private JPanel panelControlOne;
    private JLabel labelControlOneRunToProgramCounter;
    private JTextField textFieldControlOneRunToProgramCounter;
    private int intControlOneRunToProgramCounter;
    private JLabel labelControlOneRunSeveralTimes;
    private JTextField textFieldControlOneRunSeveralTimes;
    private int intControlOneRunSeveralTimes;

    private JPanel panelControlTwo;
    private JButton buttonControlTwoRunCompletely;
    private JButton buttonControlTwoRunIndividually;
    private JButton buttonControlTwoReset;
    

    public GUI(GUIManager guiManager) {
        setTitle("PIC16F8x");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setLayout(new GridLayout(1, 2));
        setSize(1850, 1000);
        setResizable(false);

        initComponents(guiManager);
        addActionListenerToButtons(guiManager);
        addComponents();

        setJMenuBar(menuBar);
        setLocationRelativeTo(null);
        setVisible(false);
    }

    public void start(GUIManager guiManager) {
        setVisible(true);
    }

    public void initComponents(GUIManager guiManager) {
        menuBar = new JMenuBar();

        menuBar.add(menuFile = new JMenu("File"));
        menuFile.add(menuItemFileOpenFile = new JMenuItem("Open"));
        stringProgrammName = null;
        menuFile.add(menuItemFileCloseFile = new JMenuItem("Close"));

        menuBar.add(menuOther = new JMenu("..."));
        menuOther.add(menuItemOtherHelp = new JMenuItem("Help"));
        optionPaneOtherHelp = new JOptionPane();
        menuOther.add(menuItemOtherInfo = new JMenuItem("Info"));
        optionPaneOtherInfo = new JOptionPane();


        panelOne = new JPanel(new GridLayout(2, 1));
        panelOne.setBorder(BorderFactory.createEmptyBorder(8, 8, 8, 4));

        panelOne.add(panelOneOne = new JPanel(new GridLayout(1, 1)));
        panelOneOne.setBorder(BorderFactory.createEmptyBorder(0, 0, 4, 0));

        panelOneOne.add(panelProgram = new JPanel(new GridLayout(1, 1)));
        panelProgram.setBorder(BorderFactory.createCompoundBorder(BorderFactory.createTitledBorder("Programm"), BorderFactory.createEmptyBorder(8, 8, 8, 8)));
        panelProgram.add(scrollPaneProgram = new JScrollPane(textAreaProgram = new JTextArea(200, 100)));
        textAreaProgram.setEditable(false);
        hightlighterProgram = textAreaProgram.getHighlighter();
        hightlightPainterProgram = new DefaultHighlighter.DefaultHighlightPainter(Color.RED);

        panelOne.add(panelOneTwo = new JPanel(new GridLayout(1, 1)));
        panelOneTwo.setBorder(BorderFactory.createEmptyBorder(4, 0, 0, 0));

        panelOneTwo.add(panelRegisterMemory = new JPanel(new GridLayout(1, 1)));
        panelRegisterMemory.setBorder(BorderFactory.createCompoundBorder(BorderFactory.createTitledBorder("Memory Register"), BorderFactory.createEmptyBorder(8, 8, 8, 8)));
        defaultTableModelRegisterMemory = new DefaultTableModel(guiManager.getPIC().getStorage().registerMemoryGetString(), new String[]{"Address", "Bank 0", "Bank 1"});
        panelRegisterMemory.add(scrollPaneRegisterMemory = new JScrollPane(tableRegisterMemory = new JTable(defaultTableModelRegisterMemory)));
        tableRegisterMemory.setEnabled(false);


        panelTwo = new JPanel(new GridLayout(3, 2));
        panelTwo.setBorder(BorderFactory.createEmptyBorder(8, 4, 8, 8));

        panelTwo.add(panelTwoOne = new JPanel(new GridLayout(1, 1)));
        panelTwoOne.setBorder(BorderFactory.createEmptyBorder(0, 0, 4, 4));

        panelTwoOne.add(panelOther = new JPanel(new GridLayout(2, 2)));
        panelOther.setBorder(BorderFactory.createCompoundBorder(BorderFactory.createTitledBorder("Other"), BorderFactory.createEmptyBorder(8, 8, 8, 8)));
        panelOther.add(labelOtherProgrammCounter = new JLabel("Program Counter:"));
        panelOther.add(labelOtherProgrammCounterValue = new JLabel(Integer.toHexString(guiManager.getPIC().getStorage().getProgramCounter()) + " [" + Integer.toString(guiManager.getPIC().getStorage().getProgramCounter()) + "]"));
        panelOther.add(labelOtherWRegister = new JLabel("W Register:"));
        panelOther.add(labelOtherWRegisterValue = new JLabel(Integer.toHexString(guiManager.getPIC().getStorage().getWRegister()) + " [" + Integer.toString(guiManager.getPIC().getStorage().getWRegister()) + "]"));

        panelTwo.add(panelTwoTwo = new JPanel(new GridLayout(1, 1)));
        panelTwoTwo.setBorder(BorderFactory.createEmptyBorder(0, 4, 4, 0));

        panelTwoTwo.add(panelRegisterStatus = new JPanel(new GridLayout(8, 2)));
        panelRegisterStatus.setBorder(BorderFactory.createCompoundBorder(BorderFactory.createTitledBorder("Status Register"), BorderFactory.createEmptyBorder(8, 8, 8, 8)));
        panelRegisterStatus.add(labelRegisterStatusC = new JLabel("C:"));
        panelRegisterStatus.add(labelRegisterStatusCValue = new JLabel(Integer.toString(guiManager.getPIC().getStorage().registerMemoryGetC())));
        panelRegisterStatus.add(labelRegisterStatusDC = new JLabel("DC:"));
        panelRegisterStatus.add(labelRegisterStatusDCValue = new JLabel(Integer.toString(guiManager.getPIC().getStorage().registerMemoryGetDC())));
        panelRegisterStatus.add(labelRegisterStatusZ = new JLabel("Z:"));
        panelRegisterStatus.add(labelRegisterStatusZValue = new JLabel(Integer.toString(guiManager.getPIC().getStorage().registerMemoryGetZ())));
        panelRegisterStatus.add(labelRegisterStatusPD = new JLabel("PD:"));
        panelRegisterStatus.add(labelRegisterStatusPDValue = new JLabel(Integer.toString(guiManager.getPIC().getStorage().registerMemoryGetPD())));
        panelRegisterStatus.add(labelRegisterStatusTO = new JLabel("TO:"));
        panelRegisterStatus.add(labelRegisterStatusTOValue = new JLabel(Integer.toString(guiManager.getPIC().getStorage().registerMemoryGetTO())));
        panelRegisterStatus.add(labelRegisterStatusRP0 = new JLabel("RP0:"));
        panelRegisterStatus.add(labelRegisterStatusRP0Value = new JLabel(Integer.toString(guiManager.getPIC().getStorage().registerMemoryGetRP0())));
        panelRegisterStatus.add(labelRegisterStatusRP1 = new JLabel("RP1:"));
        panelRegisterStatus.add(labelRegisterStatusRP1Value = new JLabel(Integer.toString(guiManager.getPIC().getStorage().registerMemoryGetRP1())));
        panelRegisterStatus.add(labelRegisterStatusIRP = new JLabel("IRP:"));
        panelRegisterStatus.add(labelRegisterStatusIRPValue = new JLabel(Integer.toString(guiManager.getPIC().getStorage().registerMemoryGetIRP())));

        panelTwo.add(panelTwoThree = new JPanel(new GridLayout(1, 1)));
        panelTwoThree.setBorder(BorderFactory.createEmptyBorder(4, 0, 4, 4));

        panelTwoThree.add(panelEightLevelStack = new JPanel(new GridLayout(8, 2)));
        panelEightLevelStack.setBorder(BorderFactory.createCompoundBorder(BorderFactory.createTitledBorder("Eight Level Stack"), BorderFactory.createEmptyBorder(8, 8, 8, 8)));
        panelEightLevelStack.add(labelEightLevelStackZero = new JLabel("0:"));
        panelEightLevelStack.add(labelEightLevelStackZeroValue = new JLabel(Integer.toHexString(guiManager.getPIC().getStorage().getEightLevelStack(0))));
        panelEightLevelStack.add(labelEightLevelStackOne = new JLabel("1:"));
        panelEightLevelStack.add(labelEightLevelStackOneValue = new JLabel(Integer.toHexString(guiManager.getPIC().getStorage().getEightLevelStack(1))));
        panelEightLevelStack.add(labelEightLevelStackTwo = new JLabel("2:"));
        panelEightLevelStack.add(labelEightLevelStackTwoValue = new JLabel(Integer.toHexString(guiManager.getPIC().getStorage().getEightLevelStack(2))));
        panelEightLevelStack.add(labelEightLevelStackThree = new JLabel("3:"));
        panelEightLevelStack.add(labelEightLevelStackThreeValue = new JLabel(Integer.toHexString(guiManager.getPIC().getStorage().getEightLevelStack(3))));
        panelEightLevelStack.add(labelEightLevelStackFour = new JLabel("4:"));
        panelEightLevelStack.add(labelEightLevelStackFourValue = new JLabel(Integer.toHexString(guiManager.getPIC().getStorage().getEightLevelStack(4))));
        panelEightLevelStack.add(labelEightLevelStackFive = new JLabel("5:"));
        panelEightLevelStack.add(labelEightLevelStackFiveValue = new JLabel(Integer.toHexString(guiManager.getPIC().getStorage().getEightLevelStack(5))));
        panelEightLevelStack.add(labelEightLevelStackSix = new JLabel("6:"));
        panelEightLevelStack.add(labelEightLevelStackSixValue = new JLabel(Integer.toHexString(guiManager.getPIC().getStorage().getEightLevelStack(6))));
        panelEightLevelStack.add(labelEightLevelStackSeven = new JLabel("7:"));
        panelEightLevelStack.add(labelEightLevelStackSevenValue = new JLabel(Integer.toHexString(guiManager.getPIC().getStorage().getEightLevelStack(7))));

        panelTwo.add(panelTwoFour = new JPanel(new GridLayout(1, 1)));
        panelTwoFour.setBorder(BorderFactory.createEmptyBorder(4, 4, 4, 0));

        panelTwoFour.add(panelPinOut = new JPanel(new GridLayout(1, 2)));
        panelPinOut.setBorder(BorderFactory.createCompoundBorder(BorderFactory.createTitledBorder("Pin Out"), BorderFactory.createEmptyBorder(8, 8, 8, 8)));
        panelPinOut.add(panelPinOutOne = new JPanel(new GridLayout(9, 2)));
        panelPinOutOne.setBorder(BorderFactory.createEmptyBorder(0, 0, 0, 4));
        panelPinOutOne.add(labelPinOutOneOSC1CLKIN = new JLabel("OSC1/CLKIN:"));
        panelPinOutOne.add(buttonPinOutOneOSC1CLKINValue = new JButton(Integer.toString(guiManager.getPIC().getStorage().watchDogTimerGetFrequency())));
        panelPinOutOne.add(labelPinOutOneOSC2CLKOUT = new JLabel("OSC2/CLKOUT:"));
        panelPinOutOne.add(buttonPinOutOneOSC2CLKOUTValue = new JButton(Integer.toString(guiManager.getPIC().getStorage().watchDogTimerGetFrequency())));
        panelPinOutOne.add(labelPinOutOneMCLR = new JLabel("MCLR:"));
        panelPinOutOne.add(buttonPinOutOneMCLRValue = new JButton(Integer.toString(guiManager.getPIC().getStorage().getPinOut(0))));
        panelPinOutOne.add(labelPinOutOneRA0 = new JLabel("RA0:"));
        panelPinOutOne.add(buttonPinOutOneRA0Value = new JButton(Integer.toString(guiManager.getPIC().getStorage().registerMemoryGetRA0())));
        panelPinOutOne.add(labelPinOutOneRA1 = new JLabel("RA1:"));
        panelPinOutOne.add(buttonPinOutOneRA1Value = new JButton(Integer.toString(guiManager.getPIC().getStorage().registerMemoryGetRA1())));
        panelPinOutOne.add(labelPinOutOneRA2 = new JLabel("RA2:"));
        panelPinOutOne.add(buttonPinOutOneRA2Value = new JButton(Integer.toString(guiManager.getPIC().getStorage().registerMemoryGetRA2())));
        panelPinOutOne.add(labelPinOutOneRA3 = new JLabel("RA3:"));
        panelPinOutOne.add(buttonPinOutOneRA3Value = new JButton(Integer.toString(guiManager.getPIC().getStorage().registerMemoryGetRA3())));
        panelPinOutOne.add(labelPinOutOneRA4T0CKI = new JLabel("RA4/T0CKI:"));
        panelPinOutOne.add(buttonPinOutOneRA4T0CKIValue = new JButton(Integer.toString(guiManager.getPIC().getStorage().registerMemoryGetRA4T0CKI())));
        panelPinOutOne.add(labelPinOutOneVSS = new JLabel("VSS:"));
        panelPinOutOne.add(buttonPinOutOneVSSValue = new JButton(Integer.toString(guiManager.getPIC().getStorage().getPinOut(1))));
        panelPinOut.add(panelPinOutTwo = new JPanel(new GridLayout(9, 2)));
        panelPinOutTwo.setBorder(BorderFactory.createEmptyBorder(0, 4, 0, 0));
        panelPinOutTwo.add(labelPinOutTwoRB0INT = new JLabel("RB0/INT:"));
        panelPinOutTwo.add(buttonPinOutTwoRB0INTValue = new JButton(Integer.toString(guiManager.getPIC().getStorage().registerMemoryGetRB0INT())));
        panelPinOutTwo.add(labelPinOutTwoRB1 = new JLabel("RB1:"));
        panelPinOutTwo.add(buttonPinOutTwoRB1Value = new JButton(Integer.toString(guiManager.getPIC().getStorage().registerMemoryGetRB1())));
        panelPinOutTwo.add(labelPinOutTwoRB2 = new JLabel("RB2:"));
        panelPinOutTwo.add(buttonPinOutTwoRB2Value = new JButton(Integer.toString(guiManager.getPIC().getStorage().registerMemoryGetRB2())));
        panelPinOutTwo.add(labelPinOutTwoRB3 = new JLabel("RB3:"));
        panelPinOutTwo.add(buttonPinOutTwoRB3Value = new JButton(Integer.toString(guiManager.getPIC().getStorage().registerMemoryGetRB3())));
        panelPinOutTwo.add(labelPinOutTwoRB4 = new JLabel("RB4:"));
        panelPinOutTwo.add(buttonPinOutTwoRB4Value = new JButton(Integer.toString(guiManager.getPIC().getStorage().registerMemoryGetRB4())));
        panelPinOutTwo.add(labelPinOutTwoRB5 = new JLabel("RB5:"));
        panelPinOutTwo.add(buttonPinOutTwoRB5Value = new JButton(Integer.toString(guiManager.getPIC().getStorage().registerMemoryGetRB5())));
        panelPinOutTwo.add(labelPinOutTwoRB6 = new JLabel("RB6:"));
        panelPinOutTwo.add(buttonPinOutTwoRB6Value = new JButton(Integer.toString(guiManager.getPIC().getStorage().registerMemoryGetRB6())));
        panelPinOutTwo.add(labelPinOutTwoRB7 = new JLabel("RB7:"));
        panelPinOutTwo.add(buttonPinOutTwoRB7Value = new JButton(Integer.toString(guiManager.getPIC().getStorage().registerMemoryGetRB7())));
        panelPinOutTwo.add(labelPinOutTwoVDD = new JLabel("VDD:"));
        panelPinOutTwo.add(buttonPinOutTwoVDDValue = new JButton(Integer.toString(guiManager.getPIC().getStorage().getPinOut(2))));

        panelTwo.add(panelTwoFive = new JPanel(new GridLayout(1, 1)));
        panelTwoFive.setBorder(BorderFactory.createEmptyBorder(4, 0, 0, 4));

        panelTwoFive.add(panelTiming = new JPanel(new GridLayout(2, 1)));
        panelTiming.setBorder(BorderFactory.createCompoundBorder(BorderFactory.createTitledBorder("Timing"), BorderFactory.createEmptyBorder(8, 8, 8, 8)));
        panelTiming.add(panelTimingOne = new JPanel(new GridLayout(4, 2)));
        panelTimingOne.add(labelTimingOneRunTime = new JLabel("Run Time:"));
        panelTimingOne.add(labelTimingOneRunTimeValue = new JLabel(Double.toString(guiManager.getPIC().getStorage().getRunTime()) + " ns" + " [" + Double.toString(guiManager.getPIC().getStorage().getRunTime() / 1000000) + " ms" + "]"));
        panelTimingOne.add(labelTimingOneTMR0 = new JLabel("TMR0:"));
        panelTimingOne.add(labelTimingOneTMR0Value = new JLabel(Integer.toHexString(guiManager.getPIC().getStorage().registerMemoryGetTMR0()) + " [" + Integer.toString(guiManager.getPIC().getStorage().registerMemoryGetTMR0()) + "]"));
        panelTimingOne.add(labelTimingOneWDT = new JLabel("WDT:"));
        panelTimingOne.add(labelTimingOneWDTValue = new JLabel(Double.toString(guiManager.getPIC().getStorage().getWatchDogTimer()) + " ns" + " [" + Double.toString(guiManager.getPIC().getStorage().getWatchDogTimer() / 1000000) + " ms" + "]"));
        panelTimingOne.add(labelTimingOneFrequency = new JLabel("Frequency:"));
        panelTimingOne.add(labelTimingOneFrequencyValue = new JLabel(Integer.toString(guiManager.getPIC().getStorage().watchDogTimerGetFrequency()) + " Hz" + " [" + Integer.toString(guiManager.getPIC().getStorage().watchDogTimerGetFrequency() / 1000) + " kHz" + "]"));
        panelTiming.add(panelTimingTwo = new JPanel(new GridLayout(1, 1)));
        panelTimingTwo.add(sliderTimingTwoFrequency = new JSlider(JSlider.HORIZONTAL, 1, 10000000, 32000));
        hashtableTimingTwoFrequency = new Hashtable<>();
        hashtableTimingTwoFrequency.put(1, new JLabel("1 Hz"));
        hashtableTimingTwoFrequency.put(5000000, new JLabel("5 mHz"));
        hashtableTimingTwoFrequency.put(10000000, new JLabel("10 mHz"));
        sliderTimingTwoFrequency.setLabelTable(hashtableTimingTwoFrequency);
        sliderTimingTwoFrequency.setMajorTickSpacing(9999999);
        sliderTimingTwoFrequency.setMinorTickSpacing(1000000);
        sliderTimingTwoFrequency.setPaintTicks(true);
        sliderTimingTwoFrequency.setPaintLabels(true);

        panelTwo.add(panelTwoSix = new JPanel(new GridLayout(1, 1)));
        panelTwoSix.setBorder(BorderFactory.createEmptyBorder(4, 4, 0, 0));

        panelTwoSix.add(panelControl = new JPanel(new GridLayout(1, 2)));
        panelControl.setBorder(BorderFactory.createCompoundBorder(BorderFactory.createTitledBorder("Control"), BorderFactory.createEmptyBorder(8, 8, 8, 8)));
        panelControl.add(panelControlOne = new JPanel(new GridLayout(2, 2)));
        panelControlOne.setBorder(BorderFactory.createEmptyBorder(0, 0, 0, 4));
        panelControlOne.add(labelControlOneRunToProgramCounter = new JLabel("Run To:"));
        panelControlOne.add(textFieldControlOneRunToProgramCounter = new JTextField());
        intControlOneRunToProgramCounter = 0;
        panelControlOne.add(labelControlOneRunSeveralTimes = new JLabel("Run Times:"));
        panelControlOne.add(textFieldControlOneRunSeveralTimes = new JTextField());
        intControlOneRunSeveralTimes = 0;
        panelControl.add(panelControlTwo = new JPanel(new GridLayout(3, 1)));
        panelControlTwo.setBorder(BorderFactory.createEmptyBorder(0, 4, 0, 0));
        panelControlTwo.add(buttonControlTwoRunCompletely = new JButton("Run Completely"));
        panelControlTwo.add(buttonControlTwoRunIndividually = new JButton("Run Individually"));
        panelControlTwo.add(buttonControlTwoReset = new JButton("Reset"));
    }

    public void addComponents() {
        add(panelOne);
        add(panelTwo);
    }

    public void refreshComponents(GUIManager guiManager) {
        /* Highlighter funktioniert warum auch immer nicht
        for (int i = 0; i < pic.getProgram().programGet().length; i++) {
            if (pic.getProgram().programGet()[i] != null) {
                if (!pic.getProgram().programGet()[i].substring(0, 4).equals("    ")) {
                    if (Integer.parseInt(pic.getProgram().programGet()[i].substring(0, 4), 16) == pic.getStorage().getProgramCounter()) {
                        hightlighterProgram.removeAllHighlights();
                        try {
                            hightlighterProgram.addHighlight(textAreaProgram.getLineStartOffset(i), textAreaProgram.getLineEndOffset(i), hightlightPainterProgram);
                        } catch (BadLocationException e) {
                            e.printStackTrace();
                        }
                        textAreaProgram.setHighlighter(hightlighterProgram);
                        break;
                    }
                }
            }
        }
        */


        defaultTableModelRegisterMemory.setDataVector(guiManager.getPIC().getStorage().registerMemoryGetString(), new String[]{"Address", "Bank 0", "Bank 1"});


        labelOtherProgrammCounterValue.setText(Integer.toHexString(guiManager.getPIC().getStorage().getProgramCounter()) + " [" + Integer.toString(guiManager.getPIC().getStorage().getProgramCounter()) + "]");
        labelOtherWRegisterValue.setText(Integer.toHexString(guiManager.getPIC().getStorage().getWRegister()) + " [" + Integer.toString(guiManager.getPIC().getStorage().getWRegister()) + "]");

        labelRegisterStatusCValue.setText(Integer.toString(guiManager.getPIC().getStorage().registerMemoryGetC()));
        labelRegisterStatusDCValue.setText(Integer.toString(guiManager.getPIC().getStorage().registerMemoryGetDC()));
        labelRegisterStatusZValue.setText(Integer.toString(guiManager.getPIC().getStorage().registerMemoryGetZ()));
        labelRegisterStatusPDValue.setText(Integer.toString(guiManager.getPIC().getStorage().registerMemoryGetPD()));
        labelRegisterStatusTOValue.setText(Integer.toString(guiManager.getPIC().getStorage().registerMemoryGetTO()));
        labelRegisterStatusRP0Value.setText(Integer.toString(guiManager.getPIC().getStorage().registerMemoryGetRP0()));
        labelRegisterStatusRP1Value.setText(Integer.toString(guiManager.getPIC().getStorage().registerMemoryGetRP1()));
        labelRegisterStatusIRPValue.setText(Integer.toString(guiManager.getPIC().getStorage().registerMemoryGetIRP()));

        labelEightLevelStackZeroValue.setText(Integer.toHexString(guiManager.getPIC().getStorage().getEightLevelStack(0)));
        labelEightLevelStackOneValue.setText(Integer.toHexString(guiManager.getPIC().getStorage().getEightLevelStack(1)));
        labelEightLevelStackTwoValue.setText(Integer.toHexString(guiManager.getPIC().getStorage().getEightLevelStack(2)));
        labelEightLevelStackThreeValue.setText(Integer.toHexString(guiManager.getPIC().getStorage().getEightLevelStack(3)));
        labelEightLevelStackFourValue.setText(Integer.toHexString(guiManager.getPIC().getStorage().getEightLevelStack(4)));
        labelEightLevelStackFiveValue.setText(Integer.toHexString(guiManager.getPIC().getStorage().getEightLevelStack(5)));
        labelEightLevelStackSixValue.setText(Integer.toHexString(guiManager.getPIC().getStorage().getEightLevelStack(6)));
        labelEightLevelStackSevenValue.setText(Integer.toHexString(guiManager.getPIC().getStorage().getEightLevelStack(7)));

        buttonPinOutOneOSC1CLKINValue.setText(Integer.toString(guiManager.getPIC().getStorage().watchDogTimerGetFrequency()));
        buttonPinOutOneOSC2CLKOUTValue.setText(Integer.toString(guiManager.getPIC().getStorage().watchDogTimerGetFrequency()));
        buttonPinOutOneMCLRValue.setText(Integer.toString(guiManager.getPIC().getStorage().getPinOut(0)));
        buttonPinOutOneRA0Value.setText(Integer.toString(guiManager.getPIC().getStorage().registerMemoryGetRA0()));
        buttonPinOutOneRA1Value.setText(Integer.toString(guiManager.getPIC().getStorage().registerMemoryGetRA1()));
        buttonPinOutOneRA2Value.setText(Integer.toString(guiManager.getPIC().getStorage().registerMemoryGetRA2()));
        buttonPinOutOneRA3Value.setText(Integer.toString(guiManager.getPIC().getStorage().registerMemoryGetRA3()));
        buttonPinOutOneRA4T0CKIValue.setText(Integer.toString(guiManager.getPIC().getStorage().registerMemoryGetRA4T0CKI()));
        buttonPinOutOneVSSValue.setText(Integer.toString(guiManager.getPIC().getStorage().getPinOut(1)));
        buttonPinOutTwoRB0INTValue.setText(Integer.toString(guiManager.getPIC().getStorage().registerMemoryGetRB0INT()));
        buttonPinOutTwoRB1Value.setText(Integer.toString(guiManager.getPIC().getStorage().registerMemoryGetRB1()));
        buttonPinOutTwoRB2Value.setText(Integer.toString(guiManager.getPIC().getStorage().registerMemoryGetRB2()));
        buttonPinOutTwoRB3Value.setText(Integer.toString(guiManager.getPIC().getStorage().registerMemoryGetRB3()));
        buttonPinOutTwoRB4Value.setText(Integer.toString(guiManager.getPIC().getStorage().registerMemoryGetRB4()));
        buttonPinOutTwoRB5Value.setText(Integer.toString(guiManager.getPIC().getStorage().registerMemoryGetRB5()));
        buttonPinOutTwoRB6Value.setText(Integer.toString(guiManager.getPIC().getStorage().registerMemoryGetRB6()));
        buttonPinOutTwoRB7Value.setText(Integer.toString(guiManager.getPIC().getStorage().registerMemoryGetRB7()));
        buttonPinOutTwoVDDValue.setText(Integer.toString(guiManager.getPIC().getStorage().getPinOut(2)));

        labelTimingOneRunTimeValue.setText(Double.toString(guiManager.getPIC().getStorage().getRunTime()) + " ns" + " [" + Double.toString(guiManager.getPIC().getStorage().getRunTime() / 1000000) + " ms" + "]");
        labelTimingOneTMR0Value.setText(Integer.toHexString(guiManager.getPIC().getStorage().registerMemoryGetTMR0()) + " [" + Integer.toString(guiManager.getPIC().getStorage().registerMemoryGetTMR0()) + "]");
        labelTimingOneWDTValue.setText(Double.toString(guiManager.getPIC().getStorage().getWatchDogTimer()) + " ns" + " [" + Double.toString(guiManager.getPIC().getStorage().getWatchDogTimer() / 1000000) + " ms" + "]");
        labelTimingOneFrequencyValue.setText(Integer.toString(guiManager.getPIC().getStorage().watchDogTimerGetFrequency()) + " Hz" + " [" + Integer.toString(guiManager.getPIC().getStorage().watchDogTimerGetFrequency() / 1000) + " kHz" + "]");
    }

    public void addActionListenerToButtons(GUIManager guiManager){
        menuItemFileOpenFile.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) { 
                fileChooserFileControlProgrammName = new JFileChooser();
                if (fileChooserFileControlProgrammName.showOpenDialog(null) == JFileChooser.APPROVE_OPTION) {
                    stringProgrammName = fileChooserFileControlProgrammName.getSelectedFile().getAbsolutePath();
                }
                guiManager.getPIC().preparation(stringProgrammName);
                guiManager.sleep(100);
                textAreaProgram.setText(null);
                for (int i = 0; i < guiManager.getPIC().getProgram().get().length; i++) {
                    if (guiManager.getPIC().getProgram().get()[i] != null) {
                        textAreaProgram.append(guiManager.getPIC().getProgram().get()[i] + "\n");
                    }
                }
                refreshComponents(guiManager);
            }
        });
        menuItemFileCloseFile.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) { 
                guiManager.getPIC().resetHard();
                guiManager.sleep(100);
                textAreaProgram.setText(null);
                textFieldControlOneRunToProgramCounter.setText(null);
                textFieldControlOneRunSeveralTimes.setText(null);
                refreshComponents(guiManager);
            }
        });

        menuItemOtherHelp.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                optionPaneOtherHelp.showMessageDialog(null, "Help Message", "Help", JOptionPane.PLAIN_MESSAGE);
            }
        });
        menuItemOtherInfo.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) { 
                optionPaneOtherInfo.showMessageDialog(null, "© Luca Matzat", "Info", JOptionPane.PLAIN_MESSAGE);
            }
        });

        buttonPinOutOneOSC1CLKINValue.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                guiManager.switchWatchDogTimerFrequency();
                guiManager.sleep(100);
                refreshComponents(guiManager);
            }
        });
        buttonPinOutOneOSC2CLKOUTValue.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                guiManager.switchWatchDogTimerFrequency();
                guiManager.sleep(100);
                refreshComponents(guiManager);
            }
        });
        buttonPinOutOneMCLRValue.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                guiManager.getPIC().getStorage().setPinOut(0);
                guiManager.getPIC().resetSoft();
                guiManager.sleep(100);
                textFieldControlOneRunToProgramCounter.setText(null);
                textFieldControlOneRunSeveralTimes.setText(null);
                refreshComponents(guiManager);
            }
        });
        buttonPinOutOneRA0Value.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (guiManager.getPIC().getStorage().pinOutGetPort(0, 0) == 0) {
                    guiManager.getPIC().getStorage().pinOutSetPort(0, 0);
                } else {
                    guiManager.getPIC().getStorage().pinOutResetPort(0, 0);
                }
                guiManager.sleep(100);
                refreshComponents(guiManager);
            }
        });
        buttonPinOutOneRA1Value.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (guiManager.getPIC().getStorage().pinOutGetPort(0, 1) == 0) {
                    guiManager.getPIC().getStorage().pinOutSetPort(0, 1);
                } else {
                    guiManager.getPIC().getStorage().pinOutResetPort(0, 1);
                }
                guiManager.sleep(100);
                refreshComponents(guiManager);
            }
        });
        buttonPinOutOneRA2Value.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (guiManager.getPIC().getStorage().pinOutGetPort(0, 2) == 0) {
                    guiManager.getPIC().getStorage().pinOutSetPort(0, 2);
                } else {
                    guiManager.getPIC().getStorage().pinOutResetPort(0, 2);
                }
                guiManager.sleep(100);
                refreshComponents(guiManager);
            }
        });
        buttonPinOutOneRA3Value.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (guiManager.getPIC().getStorage().pinOutGetPort(0, 3) == 0) {
                    guiManager.getPIC().getStorage().pinOutSetPort(0, 3);
                } else {
                    guiManager.getPIC().getStorage().pinOutResetPort(0, 3);
                }
                guiManager.sleep(100);
                refreshComponents(guiManager);
            }
        });
        buttonPinOutOneRA4T0CKIValue.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (guiManager.getPIC().getStorage().pinOutGetPort(0, 4) == 0) {
                    guiManager.getPIC().getStorage().pinOutSetPort(0, 4);
                } else {
                    guiManager.getPIC().getStorage().pinOutResetPort(0, 4);
                }
                guiManager.sleep(100);
                refreshComponents(guiManager);
            }
        });
        buttonPinOutOneVSSValue.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                guiManager.getPIC().resetHard();
                guiManager.sleep(100);
                textAreaProgram.setText(null);
                textFieldControlOneRunToProgramCounter.setText(null);
                textFieldControlOneRunSeveralTimes.setText(null);
                refreshComponents(guiManager);
            }
        });
        buttonPinOutTwoRB0INTValue.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (guiManager.getPIC().getStorage().pinOutGetPort(1, 0) == 0) {
                    guiManager.getPIC().getStorage().pinOutSetPort(1, 0);
                } else {
                    guiManager.getPIC().getStorage().pinOutResetPort(1, 0);
                }
                guiManager.sleep(100);
                refreshComponents(guiManager);
            }
        });
        buttonPinOutTwoRB1Value.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (guiManager.getPIC().getStorage().pinOutGetPort(1, 1) == 0) {
                    guiManager.getPIC().getStorage().pinOutSetPort(1, 1);
                } else {
                    guiManager.getPIC().getStorage().pinOutResetPort(1, 1);
                }
                guiManager.sleep(100);
                refreshComponents(guiManager);
            }
        });
        buttonPinOutTwoRB2Value.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (guiManager.getPIC().getStorage().pinOutGetPort(1, 2) == 0) {
                    guiManager.getPIC().getStorage().pinOutSetPort(1, 2);
                } else {
                    guiManager.getPIC().getStorage().pinOutResetPort(1, 2);
                }
                guiManager.sleep(100);
                refreshComponents(guiManager);
            }
        });
        buttonPinOutTwoRB3Value.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (guiManager.getPIC().getStorage().pinOutGetPort(1, 3) == 0) {
                    guiManager.getPIC().getStorage().pinOutSetPort(1, 3);
                } else {
                    guiManager.getPIC().getStorage().pinOutResetPort(1, 3);
                }
                guiManager.sleep(100);
                refreshComponents(guiManager);
            }
        });
        buttonPinOutTwoRB4Value.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (guiManager.getPIC().getStorage().pinOutGetPort(1, 4) == 0) {
                    guiManager.getPIC().getStorage().pinOutSetPort(1, 4);
                } else {
                    guiManager.getPIC().getStorage().pinOutResetPort(1, 4);
                }
                guiManager.sleep(100);
                refreshComponents(guiManager);
            }
        });
        buttonPinOutTwoRB5Value.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (guiManager.getPIC().getStorage().pinOutGetPort(1, 5) == 0) {
                    guiManager.getPIC().getStorage().pinOutSetPort(1, 5);
                } else {
                    guiManager.getPIC().getStorage().pinOutResetPort(1, 5);
                }
                guiManager.sleep(100);
                refreshComponents(guiManager);
            }
        });
        buttonPinOutTwoRB6Value.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (guiManager.getPIC().getStorage().pinOutGetPort(1, 6) == 0) {
                    guiManager.getPIC().getStorage().pinOutSetPort(1, 6);
                } else {
                    guiManager.getPIC().getStorage().pinOutResetPort(1, 6);
                }
                guiManager.sleep(100);
                refreshComponents(guiManager);
            }
        });
        buttonPinOutTwoRB7Value.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (guiManager.getPIC().getStorage().pinOutGetPort(1, 7) == 0) {
                    guiManager.getPIC().getStorage().pinOutSetPort(1, 7);
                } else {
                    guiManager.getPIC().getStorage().pinOutResetPort(1, 7);
                }
                guiManager.sleep(100);
                refreshComponents(guiManager);
            }
        });
        buttonPinOutTwoVDDValue.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                guiManager.getPIC().resetHard();
                guiManager.sleep(100);
                textAreaProgram.setText(null);
                textFieldControlOneRunToProgramCounter.setText(null);
                textFieldControlOneRunSeveralTimes.setText(null);
                refreshComponents(guiManager);
            }
        });

        sliderTimingTwoFrequency.addChangeListener((ChangeListener) new ChangeListener() {
            @Override
            public void stateChanged(ChangeEvent e) {
                guiManager.getPIC().getStorage().watchDogTimerSetFrequency(sliderTimingTwoFrequency.getValue());
                guiManager.sleep(10);
                refreshComponents(guiManager);
            }
          });

        textFieldControlOneRunToProgramCounter.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                intControlOneRunToProgramCounter = Integer.parseInt(textFieldControlOneRunToProgramCounter.getText(), 16);
                guiManager.getPIC().runToProgramCounter(intControlOneRunToProgramCounter);
                guiManager.sleep(100);
                textFieldControlOneRunToProgramCounter.setText(null);
                textFieldControlOneRunSeveralTimes.setText(null);
                refreshComponents(guiManager);
            }
        });
        textFieldControlOneRunSeveralTimes.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                intControlOneRunSeveralTimes = Integer.parseInt(textFieldControlOneRunSeveralTimes.getText(), 16);
                guiManager.getPIC().runSeveralTimes(intControlOneRunSeveralTimes);
                guiManager.sleep(100);
                textFieldControlOneRunToProgramCounter.setText(null);
                textFieldControlOneRunSeveralTimes.setText(null);
                refreshComponents(guiManager);
            }
        });
        buttonControlTwoRunCompletely.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                guiManager.getPIC().runCompletely();
                guiManager.sleep(100);
                refreshComponents(guiManager);
            }
        });
        buttonControlTwoRunIndividually.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                guiManager.getPIC().runIndividually();
                guiManager.sleep(100);
                refreshComponents(guiManager);
            }
        });
        buttonControlTwoReset.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                guiManager.getPIC().resetSoft();
                guiManager.sleep(100);
                sliderTimingTwoFrequency.setValue(32000);
                textFieldControlOneRunToProgramCounter.setText(null);
                textFieldControlOneRunSeveralTimes.setText(null);
                refreshComponents(guiManager);
            }
        });
    }

}
