package PIC;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Scanner;

public class Storage {

	//Program Counter
    private int programCounter;

    //Eight Level Stack
    private int[] eightLevelStack;
	private int eightLevelStackCounter;

    //Program Memory
    private int[] programMemory;

	//Register Memory (RAM)
    private int[][] registerMemory;
	private int[] RB4RB7CheckChange;

	//W Register
    private int wRegister;

	//Watch Dog Timer
    private double watchDogTimer;
	private int watchDogTimerFrequency;

	//Prescaler
    private int TMR0Prescaler;
	private int watchDogTimerPrescaler;

	//Run Time
    private double runTime;

	//Pin Out
	//0 - MCLR
	//1 - VSS
	//2 - VDD
	private int[] pinOut;
	//0 - PORTA
	//1 - PORTB
	private int[][] pinOutPORT;
	
	public Storage() {
		//Program Counter
		programCounter = 0;

        //Eight Level Stack
		eightLevelStack = new int[8];
		eightLevelStackCounter = 0;

        //Program Memory
        programMemory = new int[8000];

		//Register Memory
		registerMemory = new int[2][128];
		RB4RB7CheckChange = new int[4];

		//W Register
		wRegister = 0;

		//Watch Dog Timer
		watchDogTimer = 0;
		watchDogTimerFrequency = 0;

		//Prescaler
		TMR0Prescaler = 2;
		watchDogTimerPrescaler = 1;

		//Run Time
		runTime = 0;

		//Pin Out
		pinOut = new int[3];
		pinOutPORT = new int [2][8];
	}

	//Program Counter
	public void setProgramCounter(int value) {
		programCounter = value;
		registerMemoryRefreshPCL();
	}

	public void programCounterSetPCLATH(int value) {
		programCounter = value + registerMemoryGetPCLATH();
		registerMemoryRefreshPCL();
	}

	public void programCounterSetPCLATHPart(int value) {
		programCounter = (value + registerMemoryGetPartPCLATH()) - 1; //wird von CALL und GOTO verwendet (kSpecial 11Bit lang + 2Bit von PCLATH), -1 da der Program Counter dannach inkrementiert wird
		registerMemoryRefreshPCL();
	}

	public void countProgramCounter() {
		if (programCounter > 8191) {
			programCounter = 0;
		} else {
			programCounter++;
		}
		registerMemoryRefreshPCL();
	}

	public int getProgramCounter() {
		return programCounter;
	}
	
    //Eight Level Stack
	public void pushEightLevelStack(int value) {
		if (eightLevelStackCounter > 7) {
			eightLevelStackCounter = 0;
		}
		eightLevelStack[eightLevelStackCounter] = value;
		eightLevelStackCounter++;
	}

	public int popEightLevelStack() {
		int memory = 0;
		if (eightLevelStackCounter == 0) {
			memory = eightLevelStack[7];
			eightLevelStack[7] = 0;
			eightLevelStackCounter = 7;
		} else {
			memory = eightLevelStack[eightLevelStackCounter - 1];
			eightLevelStack[eightLevelStackCounter - 1] = 0;
			eightLevelStackCounter--;
		}
		return memory;
	}

	public int getEightLevelStack(int line) {
		return eightLevelStack[line];
	}

    //Program Memory
    public void setProgramMemory(int line, int value) {
		programMemory[line] = value;
	}

    public int getProgramMemory(int line) {
		return programMemory[line];
	}

	public int programMemoryGetSize() {
		return programMemory.length;
	}

	//Register Memory
	public void reflectRegisterMemory() {
		//INDF
		registerMemory[1][0] = registerMemory[0][0];
		//PCL
		registerMemory[1][2] = registerMemory[0][2];
		//STATUS
		registerMemory[1][3] = registerMemory[0][3];
		//FSR
		registerMemory[1][4] = registerMemory[0][4];
		//PCLATH
		registerMemory[1][10] = registerMemory[0][10];
		//INTCON
		registerMemory[1][11] = registerMemory[0][11];
		//Anwenderbereich
		for (int i = 12; i <= 47; i++) {
			registerMemory[1][i] = registerMemory[0][i];
		}
	}

	public void setRegisterMemory(int line, int value) {
		if (line == 2) { //Program Counter
			programCounterSetPCLATH(value);
		} else if ((line == 3) || (line == 4) || (line == 10) || (line == 11)) { //Gespiegelten Register
			registerMemory[0][line] = value;
		} else if ((line >= 12) && (line <= 47)) { //Anwenderbereich
			registerMemory[0][line] = value;
		} else {
			registerMemory[registerMemoryGetRP0()][line] = value;
		}
	}

	public int getRegisterMemory(int line) {
		return registerMemory[registerMemoryGetRP0()][line];
	}

	public void registerMemoryCheckInterrupt() {
		if ((registerMemoryGetGIE() == 1) && (((registerMemoryGetRBIE() == 1 ) && (registerMemoryCheckChangeRB4RB7() == 1)) || //RB4 - RB7 Ports
													((registerMemoryGetINTE() == 1) && (registerMemoryGetRB0INT() == 1)) || //RB0/INT Port
													((registerMemoryGetT0IE() == 1) && (registerMemoryGetT0IF() == 1)))) { //TMR0
			if ((registerMemoryGetRBIE() == 1 ) && (registerMemoryCheckChangeRB4RB7() == 1)) { //RB4 - RB7 Ports
				registerMemorySetRBIF();
			} else if ((registerMemoryGetINTE() == 1) && (registerMemoryGetRB0INT() == 1)) { //RB0/INT Port
				registerMemorySetINTF();
			}
			registerMemoryResetGIE();
			pushEightLevelStack(getProgramCounter());
			setProgramCounter(4 - 1); //Sprung zu ISR
		}
	}

	public int registerMemoryCheckChangeRB4RB7() {
		if (RB4RB7CheckChange[0] != pinOutGetPort(1, 4)) {
			return 1;
		} else if (RB4RB7CheckChange[1] != pinOutGetPort(1, 5)) {
			return 1;
		} else if (RB4RB7CheckChange[2] != pinOutGetPort(1, 6)) {
			return 1;
		} else if (RB4RB7CheckChange[3] != pinOutGetPort(1, 7)) {
			return 1;
		}
		RB4RB7CheckChange[0] = pinOutGetPort(1, 4);
		RB4RB7CheckChange[1] = pinOutGetPort(1, 5);
		RB4RB7CheckChange[2] = pinOutGetPort(1, 6);
		RB4RB7CheckChange[3] = pinOutGetPort(1, 7);
		return 0;
	}

	public void registerMemoryReadEEPROM() {
		int counter = 0;
		File file = new File(System.getProperty("user.home") + "\\eeprom.txt");
		Scanner scanner;
		try {
			scanner = new Scanner(file);
			while (scanner.hasNextLine() == true) {
				switch (counter) {
					case 1:
						registerMemory[0][8] = Integer.parseInt(scanner.nextLine());
						break;
					case 3:
						registerMemory[0][9] = Integer.parseInt(scanner.nextLine());
						break;
					case 5:
						registerMemory[1][8] = Integer.parseInt(scanner.nextLine());
						break;
					case 7:
						registerMemory[1][9] = Integer.parseInt(scanner.nextLine());
						break;
					default:
						scanner.nextLine();
						break;

				}
				counter++;
			}
			scanner.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
	}

	public void registerMemoryWriteEEPROM() {
		File file = new File(System.getProperty("user.home") + "\\eeprom.txt");
		BufferedWriter writer;
		try {
			writer = new BufferedWriter(new FileWriter(file));
			writer.write("EEDATA\n");
   			writer.write(Integer.toString(registerMemory[0][8]) + "\n");
			writer.write("EEADR\n");
			writer.write(Integer.toString(registerMemory[0][9]) + "\n");
			writer.write("EECON1\n");
			writer.write(Integer.toString(registerMemory[1][8]) + "\n");
			writer.write("EECON2\n");
			writer.write(Integer.toString(registerMemory[1][9]) + "\n");
   			writer.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void registerMemoryCountTMR0(PIC pic) {
		registerMemorySetTMR0(registerMemoryGetTMR0() + (1 * TMR0Prescaler));
		if ((registerMemoryGetTMR0() > 255)) {
			registerMemorySetT0IF();
			registerMemoryResetTMR0();
		}
		if ((registerMemoryGetTMR0() > 255) && (registerMemoryGetT0IE() == 1)) {
			pic.resetSoft();
		}
	}

	public void registerMemorySetTMR0(int value) {
		registerMemory[0][2] = value;
	}

	public void registerMemoryResetTMR0() {
		registerMemory[0][1] = 0;
	}

	public int registerMemoryGetTMR0() {
		return registerMemory[0][1];
	}

	public void registerMemorySetPCL(int value) {
		registerMemory[0][2] = value;
	}

	public void registerMemoryRefreshPCL() {
		registerMemory[0][2] = getProgramCounter() & 0x00FF;
	}

	public int registerMemoryGetPCL() {
		return registerMemory[0][2];
	}

	public void registerMemorySetC() {
		registerMemory[0][3] = registerMemory[0][3] | 0x0001;
	}

	public void registerMemoryResetC() {
		registerMemory[0][3] = registerMemory[0][3] & ~0x0001;
	}

	public int registerMemoryGetC() {
		if ((registerMemory[0][3] & 0x0001) > 0) {
			return 1;
		}
		return 0;
	}

	public void registerMemorySetDC() {
		registerMemory[0][3] = registerMemory[0][3] | 0x0002;
	}

	public void registerMemoryResetDC() {
		registerMemory[0][3] = registerMemory[0][3] & ~0x0002;
	}

	public int registerMemoryGetDC() {
		if ((registerMemory[0][3] & 0x0002) > 0) {
			return 1;
		}
		return 0;
	}

	public void registerMemorySetZ() {
		registerMemory[0][3] = registerMemory[0][3] | 0x0004;
	}

	public void registerMemoryResetZ() {
		registerMemory[0][3] = registerMemory[0][3] & ~0x0004;
	}

	public int registerMemoryGetZ() {
		if ((registerMemory[0][3] & 0x0004) > 0) {
			return 1;
		}
		return 0;
	}

	public void registerMemorySetPD() {
		registerMemory[0][3] = registerMemory[0][3] | 0x0008;
	}

	public void registerMemoryResetPD() {
		registerMemory[0][3] = registerMemory[0][3] & ~0x0008;
	}

	public int registerMemoryGetPD() {
		if ((registerMemory[0][3] & 0x0008) > 0) {
			return 1;
		}
		return 0;
	}

	public void registerMemorySetTO() {
		registerMemory[0][3] = registerMemory[0][3] | 0x0010;
	}

	public void registerMemoryResetTO() {
		registerMemory[0][3] = registerMemory[0][3] & ~0x0010;
	}

	public int registerMemoryGetTO() {
		if ((registerMemory[0][3] & 0x0010) > 0) {
			return 1;
		}
		return 0;
	}

	public void registerMemorySetRP0() {
		registerMemory[0][3] = registerMemory[0][3] | 0x0020;
	}

	public void registerMemoryResetRP0() {
		registerMemory[0][3] = registerMemory[0][3] & ~0x0020;
	}

	public int registerMemoryGetRP0() {
		if ((registerMemory[0][3] & 0x0020) > 0) {
			return 1;
		}
		return 0;
	}

	public void registerMemorySetRP1() {
		registerMemory[0][3] = registerMemory[0][3] | 0x0040;
	}

	public void registerMemoryResetRP1() {
		registerMemory[0][3] = registerMemory[0][3] & ~0x0040;
	}

	public int registerMemoryGetRP1() {
		if ((registerMemory[0][3] & 0x0040) > 0) {
			return 1;
		}
		return 0;
	}

	public void registerMemorySetIRP() {
		registerMemory[0][3] = registerMemory[0][3] | 0x0080;
	}

	public void registerMemoryResetIRP() {
		registerMemory[0][3] = registerMemory[0][3] & ~0x0080;
	}

	public int registerMemoryGetIRP() {
		if ((registerMemory[0][3] & 0x0080) > 0) {
			return 1;
		}
		return 0;
	}

	public void registerMemorySetRA0() {
		registerMemory[0][5] = registerMemory[0][5] | 0x0001;
	}

	public void registerMemoryResetRA0() {
		registerMemory[0][5] = registerMemory[0][5] & ~0x0001;
	}

	public int registerMemoryGetRA0() {
		if (registerMemoryGetTRISA0() == 1) {
			return pinOutGetPort(0, 0);
		} else {
			if ((registerMemory[0][5] & 0x0001) > 0) {
				return 1;
			}
		}
		return 0;
	}

	public void registerMemorySetRA1() {
		registerMemory[0][5] = registerMemory[0][5] | 0x0002;
	}

	public void registerMemoryResetRA1() {
		registerMemory[0][5] = registerMemory[0][5] & ~0x0002;
	}

	public int registerMemoryGetRA1() {
		if (registerMemoryGetTRISA1() == 1) {
			return pinOutGetPort(0, 1);
		} else {
			if ((registerMemory[0][5] & 0x0002) > 0) {
				return 1;
			}
		}
		return 0;
	}

	public void registerMemorySetRA2() {
		registerMemory[0][5] = registerMemory[0][5] | 0x0004;
	}

	public void registerMemoryResetRA2() {
		registerMemory[0][5] = registerMemory[0][5] & ~0x0004;
	}

	public int registerMemoryGetRA2() {
		if (registerMemoryGetTRISA2() == 1) {
			return pinOutGetPort(0, 2);
		} else {
			if ((registerMemory[0][5] & 0x0004) > 0) {
				return 1;
			}
		}
		return 0;
	}

	public void registerMemorySetRA3() {
		registerMemory[0][5] = registerMemory[0][5] | 0x0008;
	}

	public void registerMemoryResetRA3() {
		registerMemory[0][5] = registerMemory[0][5] & ~0x0008;
	}

	public int registerMemoryGetRA3() {
		if (registerMemoryGetTRISA3() == 1) {
			return pinOutGetPort(0, 3);
		} else {
			if ((registerMemory[0][5] & 0x0008) > 0) {
				return 1;
			}
		}
		return 0;
	}

	public void registerMemorySetRA4T0CKI() {
		registerMemory[0][5] = registerMemory[0][5] | 0x0010;
	}

	public void registerMemoryResetRA4T0CKI() {
		registerMemory[0][5] = registerMemory[0][5] & ~0x0010;
	}

	public int registerMemoryGetRA4T0CKI() {
		if (registerMemoryGetTRISA4T0CKI() == 1) {
			return pinOutGetPort(0, 4);
		} else {
			if ((registerMemory[0][5] & 0x0010) > 0) {
				return 1;
			}
		}
		return 0;
	}

	public void registerMemorySetRB0INT() {
		registerMemory[0][6] = registerMemory[0][6] | 0x0001;
	}

	public void registerMemoryResetRB0INT() {
		registerMemory[0][6] = registerMemory[0][6] & ~0x0001;
	}

	public int registerMemoryGetRB0INT() {
		if (registerMemoryGetTRISB0INT() == 1) {
			return pinOutGetPort(1, 0);
		} else {
			if ((registerMemory[0][6] & 0x0001) > 0) {
				return 1;
			}
		}
		return 0;
	}

	public void registerMemorySetRB1() {
		registerMemory[0][6] = registerMemory[0][6] | 0x0002;
	}

	public void registerMemoryResetRB1() {
		registerMemory[0][6] = registerMemory[0][6] & ~0x0002;
	}

	public int registerMemoryGetRB1() {
		if (registerMemoryGetTRISB1() == 1) {
			return pinOutGetPort(1, 1);
		} else {
			if ((registerMemory[0][6] & 0x0002) > 0) {
				return 1;
			}
		}
		return 0;
	}

	public void registerMemorySetRB2() {
		registerMemory[0][6] = registerMemory[0][6] | 0x0004;
	}

	public void registerMemoryResetRB2() {
		registerMemory[0][6] = registerMemory[0][6] & ~0x0004;
	}

	public int registerMemoryGetRB2() {
		if (registerMemoryGetTRISB2() == 1) {
			return pinOutGetPort(1, 2);
		} else {
			if ((registerMemory[0][6] & 0x0004) > 0) {
				return 1;
			}
		}
		return 0;
	}

	public void registerMemorySetRB3() {
		registerMemory[0][6] = registerMemory[0][6] | 0x0008;
	}

	public void registerMemoryResetRB3() {
		registerMemory[0][6] = registerMemory[0][6] & ~0x0008;
	}

	public int registerMemoryGetRB3() {
		if (registerMemoryGetTRISB3() == 1) {
			return pinOutGetPort(1, 3);
		} else {
			if ((registerMemory[0][6] & 0x0008) > 0) {
				return 1;
			}
		}
		return 0;
	}

	public void registerMemorySetRB4() {
		registerMemory[0][6] = registerMemory[0][6] | 0x0010;
	}

	public void registerMemoryResetRB4() {
		registerMemory[0][6] = registerMemory[0][6] & ~0x0010;
	}

	public int registerMemoryGetRB4() {
		if (registerMemoryGetTRISB4() == 1) {
			return pinOutGetPort(1, 4);
		} else {
			if ((registerMemory[0][6] & 0x0010) > 0) {
				return 1;
			}
		}
		return 0;
	}

	public void registerMemorySetRB5() {
		registerMemory[0][6] = registerMemory[0][6] | 0x0020;
	}

	public void registerMemoryResetRB5() {
		registerMemory[0][6] = registerMemory[0][6] & ~0x0020;
	}

	public int registerMemoryGetRB5() {
		if (registerMemoryGetTRISB5() == 1) {
			return pinOutGetPort(1, 5);
		} else {
			if ((registerMemory[0][6] & 0x0020) > 0) {
				return 1;
			}
		}
		return 0;
	}

	public void registerMemorySetRB6() {
		registerMemory[0][6] = registerMemory[0][6] | 0x0040;
	}

	public void registerMemoryResetRB6() {
		registerMemory[0][6] = registerMemory[0][6] & ~0x0040;
	}

	public int registerMemoryGetRB6() {
		if (registerMemoryGetTRISB6() == 1) {
			return pinOutGetPort(1, 6);
		} else {
			if ((registerMemory[0][6] & 0x0040) > 0) {
				return 1;
			}
		}
		return 0;
	}

	public void registerMemorySetRB7() {
		registerMemory[0][6] = registerMemory[0][6] | 0x0080;
	}

	public void registerMemoryResetRB7() {
		registerMemory[0][6] = registerMemory[0][6] & ~0x0080;
	}

	public int registerMemoryGetRB7() {
		if (registerMemoryGetTRISB7() == 1) {
			return pinOutGetPort(1, 7);
		} else {
			if ((registerMemory[0][6] & 0x0080) > 0) {
				return 1;
			}
		}
		return 0;
	}

	public int registerMemoryGetPCLATH() {
		return (registerMemory[0][10] & 0x001F) << 8;
	}

	public int registerMemoryGetPartPCLATH() {
		return (registerMemory[0][10] & 0x0018) << 8;
	}

	public void registerMemorySetRBIF() {
		registerMemory[0][11] = registerMemory[0][11] | 0x0001;
	}

	public void registerMemoryResetRBIF() {
		registerMemory[0][11] = registerMemory[0][11] & ~0x0001;
	}

	public int registerMemoryGetRBIF() {
		if ((registerMemory[0][11] & 0x0001) > 0) {
			return 1;
		}
		return 0;
	}

	public void registerMemorySetINTF() {
		registerMemory[0][11] = registerMemory[0][11] | 0x0002;
	}

	public void registerMemoryResetINTF() {
		registerMemory[0][11] = registerMemory[0][11] & ~0x0002;
	}

	public int registerMemoryGetINTF() {
		if ((registerMemory[0][11] & 0x0002) > 0) {
			return 1;
		}
		return 0;
	}

	public void registerMemorySetT0IF() {
		registerMemory[0][11] = registerMemory[0][11] | 0x0004;
	}

	public void registerMemoryResetT0IF() {
		registerMemory[0][11] = registerMemory[0][11] & ~0x0004;
	}

	public int registerMemoryGetT0IF() {
		if ((registerMemory[0][11] & 0x0004) > 0) {
			return 1;
		}
		return 0;
	}

	public void registerMemorySetRBIE() {
		registerMemory[0][11] = registerMemory[0][11] | 0x0008;
	}

	public void registerMemoryResetRBIE() {
		registerMemory[0][11] = registerMemory[0][11] & ~0x0008;
	}

	public int registerMemoryGetRBIE() {
		if ((registerMemory[0][11] & 0x0008) > 0) {
			return 1;
		}
		return 0;
	}

	public void registerMemorySetINTE() {
		registerMemory[0][11] = registerMemory[0][11] | 0x0010;
	}

	public void registerMemoryResetINTE() {
		registerMemory[0][11] = registerMemory[0][11] & ~0x0010;
	}

	public int registerMemoryGetINTE() {
		if ((registerMemory[0][11] & 0x0010) > 0) {
			return 1;
		}
		return 0;
	}

	public void registerMemorySetT0IE() {
		registerMemory[0][11] = registerMemory[0][11] | 0x0020;
	}

	public void registerMemoryResetT0IE() {
		registerMemory[0][11] = registerMemory[0][11] & ~0x0020;
	}

	public int registerMemoryGetT0IE() {
		if ((registerMemory[0][11] & 0x0020) > 0) {
			return 1;
		}
		return 0;
	}

	public void registerMemorySetEEIE() {
		registerMemory[0][11] = registerMemory[0][11] | 0x0040;
	}

	public void registerMemoryResetEEIE() {
		registerMemory[0][11] = registerMemory[0][11] & ~0x0040;
	}

	public int registerMemoryGetEEIE() {
		if ((registerMemory[0][11] & 0x0040) > 0) {
			return 1;
		}
		return 0;
	}

	public void registerMemorySetGIE() {
		registerMemory[0][11] = registerMemory[0][11] | 0x0080;
	}

	public void registerMemoryResetGIE() {
		registerMemory[0][11] = registerMemory[0][11] & ~0x0080;
	}

	public int registerMemoryGetGIE() {
		if ((registerMemory[0][11] & 0x0080) > 0) {
			return 1;
		}
		return 0;
	}

	public void registerMemorySetPS0() {
		registerMemory[1][1] = registerMemory[1][1] | 0x0001;
	}

	public void registerMemoryResetPS0() {
		registerMemory[1][1] = registerMemory[1][1] & ~0x0001;
	}

	public int registerMemoryGetPS0() {
		if ((registerMemory[1][1] & 0x0001) > 0) {
			return 1;
		}
		return 0;
	}

	public void registerMemorySetPS1() {
		registerMemory[1][1] = registerMemory[1][1] | 0x0002;
	}

	public void registerMemoryResetPS1() {
		registerMemory[1][1] = registerMemory[1][1] & ~0x0002;
	}

	public int registerMemoryGetPS1() {
		if ((registerMemory[1][1] & 0x0002) > 0) {
			return 1;
		}
		return 0;
	}

	public void registerMemorySetPS2() {
		registerMemory[1][1] = registerMemory[1][1] | 0x0004;
	}

	public void registerMemoryResetPS2() {
		registerMemory[1][1] = registerMemory[1][1] & ~0x0004;
	}

	public int registerMemoryGetPS2() {
		if ((registerMemory[1][1] & 0x0004) > 0) {
			return 1;
		}
		return 0;
	}

	public void registerMemorySetPSA() {
		registerMemory[1][1] = registerMemory[1][1] | 0x0008;
	}

	public void registerMemoryResetPSA() {
		registerMemory[1][1] = registerMemory[1][1] & ~0x0008;
	}

	public int registerMemoryGetPSA() {
		if ((registerMemory[1][1] & 0x0008) > 0) {
			return 1;
		}
		return 0;
	}

	public void registerMemorySetT0SE() {
		registerMemory[1][1] = registerMemory[1][1] | 0x0010;
	}

	public void registerMemoryResetT0SE() {
		registerMemory[1][1] = registerMemory[1][1] & ~0x0010;
	}

	public int registerMemoryGetT0SE() {
		if ((registerMemory[1][1] & 0x0010) > 0) {
			return 1;
		}
		return 0;
	}

	public void registerMemorySetT0CS() {
		registerMemory[1][1] = registerMemory[1][1] | 0x0020;
	}

	public void registerMemoryResetT0CS() {
		registerMemory[1][1] = registerMemory[1][1] & ~0x0020;
	}

	public int registerMemoryGetT0CS() {
		if ((registerMemory[1][1] & 0x0020) > 0) {
			return 1;
		}
		return 0;
	}

	public void registerMemorySetINTEDG() {
		registerMemory[1][1] = registerMemory[1][1] | 0x0040;
	}

	public void registerMemoryResetINTEDG() {
		registerMemory[1][1] = registerMemory[1][1] & ~0x0040;
	}

	public int registerMemoryGetINTEDG() {
		if ((registerMemory[1][1] & 0x0040) > 0) {
			return 1;
		}
		return 0;
	}

	public void registerMemorySetRBPU() {
		registerMemory[1][1] = registerMemory[1][1] | 0x0080;
	}

	public void registerMemoryResetRBPU() {
		registerMemory[1][1] = registerMemory[1][1] & ~0x0080;
	}

	public int registerMemoryGetRBPU() {
		if ((registerMemory[1][1] & 0x0080) > 0) {
			return 1;
		}
		return 0;
	}

	public void registerMemorySetTRISA0() {
		registerMemory[1][5] = registerMemory[1][5] | 0x0001;
	}

	public void registerMemoryResetTRISA0() {
		registerMemory[1][5] = registerMemory[1][5] & ~0x0001;
	}

	public int registerMemoryGetTRISA0() {
		if ((registerMemory[1][5] & 0x0001) > 0) {
			return 1;
		}
		return 0;
	}

	public void registerMemorySetTRISA1() {
		registerMemory[1][5] = registerMemory[1][5] | 0x0002;
	}

	public void registerMemoryResetTRISA1() {
		registerMemory[1][5] = registerMemory[1][5] & ~0x0002;
	}

	public int registerMemoryGetTRISA1() {
		if ((registerMemory[1][5] & 0x0002) > 0) {
			return 1;
		}
		return 0;
	}

	public void registerMemorySetTRISA2() {
		registerMemory[1][5] = registerMemory[1][5] | 0x0004;
	}

	public void registerMemoryResetTRISA2() {
		registerMemory[1][5] = registerMemory[1][5] & ~0x0004;
	}

	public int registerMemoryGetTRISA2() {
		if ((registerMemory[1][5] & 0x0004) > 0) {
			return 1;
		}
		return 0;
	}

	public void registerMemorySetTRISA3() {
		registerMemory[1][5] = registerMemory[1][5] | 0x0008;
	}

	public void registerMemoryResetTRISA3() {
		registerMemory[1][5] = registerMemory[1][5] & ~0x0008;
	}

	public int registerMemoryGetTRISA3() {
		if ((registerMemory[1][5] & 0x0008) > 0) {
			return 1;
		}
		return 0;
	}

	public void registerMemorySetTRISA4T0CKI() {
		registerMemory[1][5] = registerMemory[1][5] | 0x0010;
	}

	public void registerMemoryResetTRISA4T0CKI() {
		registerMemory[1][5] = registerMemory[1][5] & ~0x0010;
	}

	public int registerMemoryGetTRISA4T0CKI() {
		if ((registerMemory[1][5] & 0x0010) > 0) {
			return 1;
		}
		return 0;
	}

	public void registerMemorySetTRISB0INT() {
		registerMemory[1][6] = registerMemory[1][6] | 0x0001;
	}

	public void registerMemoryResetTRISB0INT() {
		registerMemory[1][6] = registerMemory[1][6] & ~0x0001;
	}

	public int registerMemoryGetTRISB0INT() {
		if ((registerMemory[1][6] & 0x0001) > 0) {
			return 1;
		}
		return 0;
	}

	public void registerMemorySetTRISB1() {
		registerMemory[1][6] = registerMemory[1][6] | 0x0002;
	}

	public void registerMemoryResetTRISB1() {
		registerMemory[1][6] = registerMemory[1][6] & ~0x0002;
	}

	public int registerMemoryGetTRISB1() {
		if ((registerMemory[1][6] & 0x0002) > 0) {
			return 1;
		}
		return 0;
	}

	public void registerMemorySetTRISB2() {
		registerMemory[1][6] = registerMemory[1][6] | 0x0004;
	}

	public void registerMemoryResetTRISB2() {
		registerMemory[1][6] = registerMemory[1][6] & ~0x0004;
	}

	public int registerMemoryGetTRISB2() {
		if ((registerMemory[1][6] & 0x0004) > 0) {
			return 1;
		}
		return 0;
	}

	public void registerMemorySetTRISB3() {
		registerMemory[1][6] = registerMemory[1][6] | 0x0008;
	}

	public void registerMemoryResetTRISB3() {
		registerMemory[1][6] = registerMemory[1][6] & ~0x0008;
	}

	public int registerMemoryGetTRISB3() {
		if ((registerMemory[1][6] & 0x0008) > 0) {
			return 1;
		}
		return 0;
	}

	public void registerMemorySetTRISB4() {
		registerMemory[1][6] = registerMemory[1][6] | 0x0010;
	}

	public void registerMemoryResetTRISB4() {
		registerMemory[1][6] = registerMemory[1][6] & ~0x0010;
	}

	public int registerMemoryGetTRISB4() {
		if ((registerMemory[1][6] & 0x0010) > 0) {
			return 1;
		}
		return 0;
	}

	public void registerMemorySetTRISB5() {
		registerMemory[1][6] = registerMemory[1][6] | 0x0020;
	}

	public void registerMemoryResetTRISB5() {
		registerMemory[1][6] = registerMemory[1][6] & ~0x0020;
	}

	public int registerMemoryGetTRISB5() {
		if ((registerMemory[1][6] & 0x0020) > 0) {
			return 1;
		}
		return 0;
	}

	public void registerMemorySetTRISB6() {
		registerMemory[1][6] = registerMemory[1][6] | 0x0040;
	}

	public void registerMemoryResetTRISB6() {
		registerMemory[1][6] = registerMemory[1][6] & ~0x0040;
	}

	public int registerMemoryGetTRISB6() {
		if ((registerMemory[1][6] & 0x0040) > 0) {
			return 1;
		}
		return 0;
	}

	public void registerMemorySetTRISB7() {
		registerMemory[1][6] = registerMemory[1][6] | 0x0080;
	}

	public void registerMemoryResetTRISB7() {
		registerMemory[1][6] = registerMemory[1][6] & ~0x0080;
	}

	public int registerMemoryGetTRISB7() {
		if ((registerMemory[1][6] & 0x0080) > 0) {
			return 1;
		}
		return 0;
	}

	public void registerMemorySetRD() {
		registerMemory[1][8] = registerMemory[1][8] | 0x0001;
	}

	public void registerMemoryResetRD() {
		registerMemory[1][8] = registerMemory[1][8] & ~0x0001;
	}

	public int registerMemoryGetRD() {
		if ((registerMemory[1][8] & 0x0001) > 0) {
			return 1;
		}
		return 0;
	}

	public void registerMemorySetWR() {
		registerMemory[1][8] = registerMemory[1][8] | 0x0002;
	}

	public void registerMemoryResetWR() {
		registerMemory[1][8] = registerMemory[1][8] & ~0x0002;
	}

	public int registerMemoryGetWR() {
		if ((registerMemory[1][8] & 0x0002) > 0) {
			return 1;
		}
		return 0;
	}

	public void registerMemorySetWREN() {
		registerMemory[1][8] = registerMemory[1][8] | 0x0004;
	}

	public void registerMemoryResetWREN() {
		registerMemory[1][8] = registerMemory[1][8] & ~0x0004;
	}

	public int registerMemoryGetWREN() {
		if ((registerMemory[1][8] & 0x0004) > 0) {
			return 1;
		}
		return 0;
	}

	public void registerMemorySetWRERR() {
		registerMemory[1][8] = registerMemory[1][8] | 0x0008;
	}

	public void registerMemoryResetWRERR() {
		registerMemory[1][8] = registerMemory[1][8] & ~0x0008;
	}

	public int registerMemoryGetWRERR() {
		if ((registerMemory[1][8] & 0x0008) > 0) {
			return 1;
		}
		return 0;
	}

	public void registerMemorySetEEIF() {
		registerMemory[1][8] = registerMemory[1][8] | 0x0010;
	}

	public void registerMemoryResetEEIF() {
		registerMemory[1][8] = registerMemory[1][8] & ~0x0010;
	}

	public int registerMemoryGetEEIF() {
		if ((registerMemory[1][8] & 0x0010) > 0) {
			return 1;
		}
		return 0;
	}

	public String[][] registerMemoryGetString() {
		String[][] registerMemoryString = new String[128][3];
		for (int i = 0; i < 128; i++) {
			registerMemoryString[i][0] = Integer.toHexString(i);
		}
		for (int i = 0; i < 128; i++) {
			if (i < 12) {
				switch (i) {
					case 0:
						registerMemoryString[i][1] = Integer.toHexString(registerMemory[0][i]) + " [INDF]";
						break;
					case 1:
						registerMemoryString[i][1] = Integer.toHexString(registerMemory[0][i]) + " [TMR0]";
						break;
					case 2:
						registerMemoryString[i][1] = Integer.toHexString(registerMemory[0][i]) + " [PCL]";
						break;
					case 3:
						registerMemoryString[i][1] = Integer.toHexString(registerMemory[0][i]) + " [STATUS]";
						break;
					case 4:
						registerMemoryString[i][1] = Integer.toHexString(registerMemory[0][i]) + " [FSR]";
						break;
					case 5:
						registerMemoryString[i][1] = Integer.toHexString(registerMemory[0][i]) + " [PORTA]";
						break;
					case 6:
						registerMemoryString[i][1] = Integer.toHexString(registerMemory[0][i]) + " [PORTB]";
						break;
					case 7:
						registerMemoryString[i][1] = Integer.toHexString(registerMemory[0][i]);
						break;
					case 8:
						registerMemoryString[i][1] = Integer.toHexString(registerMemory[0][i]) + " [EEDATA]";
						break;
					case 9:
						registerMemoryString[i][1] = Integer.toHexString(registerMemory[0][i]) + " [EEADR]";
						break;
					case 10:
						registerMemoryString[i][1] = Integer.toHexString(registerMemory[0][i]) + " [PCLATH]";
						break;
					case 11:
						registerMemoryString[i][1] = Integer.toHexString(registerMemory[0][i]) + " [INTCON]";
						break;
					default:
						break;
				}
			} else {
				registerMemoryString[i][1] = Integer.toHexString(registerMemory[0][i]);
			}
		}
		for (int i = 0; i < 128; i++) {
			if (i < 12) {
				switch (i) {
					case 0:
						registerMemoryString[i][2] = Integer.toHexString(registerMemory[1][i]) + " [INDF]";
						break;
					case 1:
						registerMemoryString[i][2] = Integer.toHexString(registerMemory[1][i]) + " [OPTION_REG]";
						break;
					case 2:
						registerMemoryString[i][2] = Integer.toHexString(registerMemory[1][i]) + " [PCL]";
						break;
					case 3:
						registerMemoryString[i][2] = Integer.toHexString(registerMemory[1][i]) + " [STATUS]";
						break;
					case 4:
						registerMemoryString[i][2] = Integer.toHexString(registerMemory[1][i]) + " [FSR]";
						break;
					case 5:
						registerMemoryString[i][2] = Integer.toHexString(registerMemory[1][i]) + " [TRISA]";
						break;
					case 6:
						registerMemoryString[i][2] = Integer.toHexString(registerMemory[1][i]) + " [TRISB]";
						break;
					case 7:
						registerMemoryString[i][2] = Integer.toHexString(registerMemory[1][i]);
						break;
					case 8:
						registerMemoryString[i][2] = Integer.toHexString(registerMemory[1][i]) + " [EECON1]";
						break;
					case 9:
						registerMemoryString[i][2] = Integer.toHexString(registerMemory[1][i]) + " [EECON2]";
						break;
					case 10:
						registerMemoryString[i][2] = Integer.toHexString(registerMemory[1][i]) + " [PCLATH]";
						break;
					case 11:
						registerMemoryString[i][2] = Integer.toHexString(registerMemory[1][i]) + " [INTCON]";
						break;
					default:
						break;
				}
			} else {
				registerMemoryString[i][2] = Integer.toHexString(registerMemory[1][i]);
			}
		}
		return registerMemoryString;
	}

	//W Register
	public void setWRegister(int value) {
		wRegister = value;
	}

	public int getWRegister() {
		return wRegister;
	}

	//Watch Dog Timer
	public void countWatchDogTimer() {
		watchDogTimer = watchDogTimer + (1000000000 / (watchDogTimerFrequency));
		if (watchDogTimer > (18000000 * watchDogTimerPrescaler)) {
			registerMemorySetTO();
		}
	}

	public void resetWatchDogTimer() {
		watchDogTimer = 0;
	}

	public double getWatchDogTimer() {
		return watchDogTimer;
	}

	public void watchDogTimerSetFrequency(int frequency) {
		watchDogTimerFrequency = frequency;
	}

	public int watchDogTimerGetFrequency() {
		return watchDogTimerFrequency;
	}

	//Prescaler
	public void setPrescaler() {
		if ((registerMemory[1][1] & 0x0008) == 0) {
			switch (registerMemory[1][1] & 0x0007) {
				case 0:
					TMR0Prescaler = 2;
					break;
				case 1:
					TMR0Prescaler = 4;
					break;
				case 2:
					TMR0Prescaler = 8;
					break;
				case 3:
					TMR0Prescaler = 16;
					break;
				case 4:
					TMR0Prescaler = 32;
					break;
				case 5:
					TMR0Prescaler = 64;
					break;
				case 6:
					TMR0Prescaler = 128;
					break;
				case 7:
					TMR0Prescaler = 256;
					break;
				default:
					break;
			}
		} else if ((registerMemory[1][1] & 0x0008) > 0) {
			switch (registerMemory[1][1] & 0x0007) {
				case 0:
					watchDogTimerPrescaler = 1;
					break;
				case 1:
					watchDogTimerPrescaler = 2;
					break;
				case 2:
					watchDogTimerPrescaler = 4;
					break;
				case 3:
					watchDogTimerPrescaler = 8;
					break;
				case 4:
					watchDogTimerPrescaler = 16;
					break;
				case 5:
					watchDogTimerPrescaler = 32;
					break;
				case 6:
					watchDogTimerPrescaler = 64;
					break;
				case 7:
					watchDogTimerPrescaler = 128;
					break;
				default:
					break;
			}
		}
	}

	public void resetPrescaler() {
		registerMemoryResetPS0();
        registerMemoryResetPS1();
        registerMemoryResetPS2();
		TMR0Prescaler = 2;
		watchDogTimerPrescaler = 1;
	}

	//Run Time
    public void countRunTime() {
		runTime = runTime + (1000000000 / (watchDogTimerFrequency));
	}

	public double getRunTime() {
		return runTime;
	}

	//Pin Out
	public void setPinOut(int line) {
		pinOut[line] = 1;
	}

	public void resetPinOut(int line) {
		pinOut[line] = 0;
	}

	public int getPinOut(int line) {
		return pinOut[line];
	}

	public void pinOutSetPort(int bank, int line) {
		pinOutPORT[bank][line] = 1;
	}

	public void pinOutResetPort(int bank, int line) {
		pinOutPORT[bank][line] = 0;
	}

	public int pinOutGetPort(int bank, int line) {
		return pinOutPORT[bank][line];
	}
}