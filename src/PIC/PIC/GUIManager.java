package PIC;

public class GUIManager {
    private PIC pic;

    public GUIManager() {
        pic = new PIC();
    }

    public void start() {
        GUI gui = new GUI(this);
        gui.start(this);
    }

    public PIC getPIC() {
        return pic;
    }

    public void sleep(int value) {
        try {
            Thread.sleep(value);
        } catch (InterruptedException e1) {
            e1.printStackTrace();
        }
    }

    public void switchWatchDogTimerFrequency() {
        if ((pic.getStorage().watchDogTimerGetFrequency() >= 1) && (pic.getStorage().watchDogTimerGetFrequency() < 1000)) {
            pic.getStorage().watchDogTimerSetFrequency(1000);
        } else if ((pic.getStorage().watchDogTimerGetFrequency() >= 1000) && (pic.getStorage().watchDogTimerGetFrequency() < 10000)) {
            pic.getStorage().watchDogTimerSetFrequency(10000);
        } else if ((pic.getStorage().watchDogTimerGetFrequency() >= 10000) && (pic.getStorage().watchDogTimerGetFrequency() < 100000)) {
            pic.getStorage().watchDogTimerSetFrequency(100000);
        } else if ((pic.getStorage().watchDogTimerGetFrequency() >= 100000) && (pic.getStorage().watchDogTimerGetFrequency() < 1000000)) {
            pic.getStorage().watchDogTimerSetFrequency(1000000);
        } else if ((pic.getStorage().watchDogTimerGetFrequency() >= 1000000) && (pic.getStorage().watchDogTimerGetFrequency() < 10000000)) {
            pic.getStorage().watchDogTimerSetFrequency(10000000);
        } else {
            pic.getStorage().watchDogTimerSetFrequency(1);
        }
    }
}
