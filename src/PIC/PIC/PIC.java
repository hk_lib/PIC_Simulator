package PIC;

public class PIC {
    
    private Calculator calculator;
	private Storage storage;
    private Program programm;
    private int[] checkEnd;
    private int checkEndCounter;

    public PIC() {
        calculator = new Calculator();
        storage = new Storage();
        programm = new Program(storage);
        checkEnd = new int[10];
        checkEndCounter = 0;
    }

    public void preparation(String programmName) {
        resetHard();
        storage.registerMemoryReadEEPROM();
        storage.registerMemorySetPD();
        storage.registerMemorySetTO();
        storage.registerMemorySetPS0();
        storage.registerMemorySetPS1();
        storage.registerMemorySetPS2();
        storage.registerMemorySetPSA();
        storage.registerMemorySetT0SE();
        storage.registerMemorySetT0CS();
        storage.registerMemorySetINTEDG();
        storage.registerMemorySetRBPU();
        storage.registerMemorySetTRISA0();
        storage.registerMemorySetTRISA1();
        storage.registerMemorySetTRISA2();
        storage.registerMemorySetTRISA3();
        storage.registerMemorySetTRISA4T0CKI();
        storage.registerMemorySetTRISB0INT();
        storage.registerMemorySetTRISB1();
        storage.registerMemorySetTRISB2();
        storage.registerMemorySetTRISB3();
        storage.registerMemorySetTRISB4();
        storage.registerMemorySetTRISB5();
        storage.registerMemorySetTRISB6();
        storage.registerMemorySetTRISB7();
        storage.reflectRegisterMemory();
        storage.watchDogTimerSetFrequency(32000);
        storage.setPinOut(1);
        storage.setPinOut(2);
        programm.read(programmName);
        programm.divideIntoStorage(storage);
    }

    public void runCompletely() {
        while (storage.getProgramCounter() < storage.programMemoryGetSize()) {
            if (checkEnd() == 1) {
                return;
            }
            runIndividually();
        }
    }

    public void runIndividually() {
        if ((checkEnd() == 1) || (storage.getProgramCounter() >= storage.programMemoryGetSize())) {
            return;
        }
        calculator.calculate(this, storage, storage.getProgramMemory(storage.getProgramCounter()));
        storage.countProgramCounter();
        storage.reflectRegisterMemory();
    }

    public void runToProgramCounter(int value) {
        if (value >= storage.programMemoryGetSize()) {
            return;
        }
        while (storage.getProgramCounter() != value) {
            if (checkEnd() == 1) {
                return;
            }
            runIndividually();
        }
    }

    public void runSeveralTimes(int value) {
        for (int i = 0; i < value; i++) {
            if ((checkEnd() == 1) || (storage.getProgramCounter() >= storage.programMemoryGetSize())) {
                return;
            }
            runIndividually();
        }
    }

    public int checkEnd() { //Um die Endlosschleife (goto end) zu beenden
        checkEnd[checkEndCounter] = storage.getProgramCounter();
        int memory = 1;
        for (int i = 0; i < checkEnd.length; i++) {
            if (i == 0) {
                if ((checkEnd[0] != checkEnd[9]) || (checkEnd[0] == 0)) {
                    memory = 0;
                    break;
                }
            } else {
                if ((checkEnd[i] != checkEnd[i - 1]) || (checkEnd[i] == 0)) {
                    memory = 0;
                    break;
                }
            }
        }
        if (checkEndCounter == 9) {
            checkEndCounter = 0;
        } else {
            checkEndCounter++;
        }
        return memory;
    }

    public void resetSoft() {
        storage.registerMemoryWriteEEPROM();
        calculator = new Calculator();
        storage = new Storage();
        programm = new Program(storage);
        checkEnd = new int[10];
        checkEndCounter = 0;
    }

    public void resetHard() {
        storage.registerMemoryWriteEEPROM();
        storage = new Storage();
        storage.registerMemoryReadEEPROM();
        storage.registerMemorySetPS0();
        storage.registerMemorySetPS1();
        storage.registerMemorySetPS2();
        storage.registerMemorySetPSA();
        storage.registerMemorySetT0SE();
        storage.registerMemorySetT0CS();
        storage.registerMemorySetINTEDG();
        storage.registerMemorySetRBPU();
        storage.registerMemorySetTRISA0();
        storage.registerMemorySetTRISA1();
        storage.registerMemorySetTRISA2();
        storage.registerMemorySetTRISA3();
        storage.registerMemorySetTRISA4T0CKI();
        storage.registerMemorySetTRISB0INT();
        storage.registerMemorySetTRISB1();
        storage.registerMemorySetTRISB2();
        storage.registerMemorySetTRISB3();
        storage.registerMemorySetTRISB4();
        storage.registerMemorySetTRISB5();
        storage.registerMemorySetTRISB6();
        storage.registerMemorySetTRISB7();
        storage.reflectRegisterMemory();
        storage.watchDogTimerSetFrequency(32000);
        storage.setPinOut(1);
        storage.setPinOut(2);
        programm.divideIntoStorage(storage);
        checkEnd = new int[10];
        checkEndCounter = 0;
    }

    public Calculator getCalculator() {
        return calculator;
    }

    public Storage getStorage() {
        return storage;
    }

    public Program getProgram() {
        return programm;
    }
}
