package PIC;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class Program {
    
    private String[] program;

    public Program(Storage storage) {
        program = new String[storage.programMemoryGetSize()];
    }

    public void read(String programName) {
		int counter = 1;
		program[0] = null;
		File file = new File(programName);
		Scanner scanner;
        try {
            scanner = new Scanner(file);
            while (scanner.hasNextLine() == true) {
                program[counter] = scanner.nextLine();
                counter++;
            }
            scanner.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
	}

    public void divideIntoStorage(Storage storage) {
        String[][] programDivided = new String[storage.programMemoryGetSize()][2];
        for (int i = 1; i < program.length; i++) {
            if (program[i] != null) {
                programDivided[i][0] = program[i].substring(0, 4);
            }
        }
        for (int i = 1; i < program.length; i++) {
            if (program[i] != null) {
                programDivided[i][1] = program[i].substring(5, 9);
            }
        }

        //Fill Storage
        for (int i = 0; i < storage.programMemoryGetSize(); i++) {
            if (programDivided[i][0] != null) {
                if (!programDivided[i][0].equals("    ")) {
                    storage.setProgramMemory(Integer.parseInt(programDivided[i][0], 16), Integer.parseInt(programDivided[i][1], 16));
                }
            }
        }
    }

    public String[] get() {
        return program;
    }

}
