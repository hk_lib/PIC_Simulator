package PIC;

public class Calculator {
    // carry bit: überlaufsbit bsp 255 + 1 = 0; c=1             
    // digit carry: überlaufsbit der unteren 4 bits                                                                  bit1
    // zero flag: ist gesetzt wenn ergebnis einer operation 0 ist                                                    bit2    
    // power down bit: sleep befehl-> 0 ; einschalten des pics oder clrwdt befehl -> 1                               bit3
    // time out bit: wenn watchdog abgeflaufen ist -> 0; beim einschalten des pics / sleep / clrwdt -> 1;            bit4
    // rp0 bit: registerbänke werden umgeschalten: wenn 0 -> Bank 0; wenn 1 -> Bank 1;                               bit5 (6)
    //IRP: wird nur bei PICS mit mehr als 2 Bänken benötigt                                                          bit(7)
    //FSR  file select register: RAM adresse 4  8bit gros     zeiger auf verwendete speicheradresse                  

    private int CAdd;
    private int CSubtract;
    private int DCAdd;
    private int DCSubtract;
    
    public Calculator() {
        CAdd = 0;
        CSubtract = 0;
        DCAdd = 0;
        DCSubtract = 0;
    }

    public void calculate(PIC pic, Storage storage, int commandValue) {
        int b = commandValue & 0x0380;// 11 1000 0000
        int d = commandValue & 0x0080;// 1000 0000
        int f = commandValue & 0x007F;// 111 1111
        int k = commandValue & 0x00FF;// 1111 1111
        int kSpecial = commandValue & 0x07FF; //mehr bit, bei call und goto
        int fValue = 0;
        int wValue = storage.getWRegister();
        int result = 0;
        int memory = 0;

        if (f == 0) { //FSR (Zeigerfunktion)
            f = storage.getRegisterMemory(4);
        }
        fValue = storage.getRegisterMemory(f);

        //ADDWF
        if ((commandValue >= 1792) && (commandValue <= 2047)) { // 111 dfff ffff // 111 0000 0000 bis 111 1111 1111
            result = add(wValue, fValue);                       // Add wValue and fValue
            carryFlag(storage);                                 // W + f = result
            digitCarryFlag(storage);
            resetAddSubFlag();
            zeroFlag(storage, result);
            storeValue(storage, d, f, result);                  // if d = 0 store result in wValue if d = 1 store result in fValue
        }

        //ANDWF
        if ((commandValue >= 1280) && (commandValue <= 1535)) { // 101 dfff ffff // 101 0000 0000 bis 101 1111 1111
            result = wValue & fValue;                           // AND wValue with fValue
            zeroFlag(storage, result);                          // 
            storeValue(storage, d, f, result);
        }

        //CLRF
        if ((commandValue >= 384) && (commandValue <= 511)) {   // 1 1fff ffff //1 1000 0000 bis 1 1111 1111
            result = 0;                                         // Bit 'b' in register f ist gelöscht 
            zeroFlag(storage, result);
            storage.setRegisterMemory(f, result);
        }

        //CLRW
        if ((commandValue >= 256) && (commandValue <= 383)) {   // 1 0xxx xxxx //1 0000 0000 bis 1 0111 1111    
            result = 0;                                        //  W register is cleared. Zero bit (Z) is set
            zeroFlag(storage, result);
            storage.setWRegister(result);
        }

        //COMF
        if ((commandValue >= 2304) && (commandValue <= 2559)) { // 1001 dfff ffff //1001 0000 0000 bis 1001 1111 1111
            result = ~fValue;                                   // The contents of register ’f’ are complemented. If ’d’ is 0 the result is stored in 
            zeroFlag(storage, result);                          // W. If ’d’ is 1 the result is stored back in register ’f’.
            storeValue(storage, d, f, result);
        }

        //DECF
        if ((commandValue >= 768) && (commandValue <= 1023)) {  // 11 dfff ffff //11 0000 0000 bis 11 1111 1111
            result = subtract(fValue, 1);              // Decrement contents of register ’f’. If ’d’ is 0 the result is stored in the W register.
            zeroFlag(storage, result);                          //  If ’d’ is 1 the result is stored back in register ’f’. 
            storeValue(storage, d, f, result);
        }

        //DECFSZ
        if ((commandValue >= 2816) && (commandValue <= 3071)) {  // 1011 dfff ffff // 1011 0000 0000 bis 1011 1111 1111
            result = subtract(fValue, 1);              // The contents of register ’f’ are decremented. If ’d’ is 0 the result is placed in the W register. 
            if (result == 0) {                                  // If ’d’ is 1 the result is placed back in register ’f’. 
                storage.countProgramCounter();                  // If the result is not 0, the next instruction, is executed. If the result is 0, then a NOP is executed instead making it a 2TCY instruction.
            }
            storeValue(storage, d, f, result);
        }

        //INCF
        if ((commandValue >= 2560) && (commandValue <= 2815)) { // 1010 dfff ffff //1010 0000 0000 bis 1010 1111 1111
            result = add(fValue, 1);                  // The contents of register ’f’ are incremented. If ’d’ is 0 the result is placed in the W register. 
            zeroFlag(storage, result);                          // If ’d’ is 1 the result is placed back in register ’f’.
            storeValue(storage, d, f, result);  
        }

        //INCFSZ
        if ((commandValue >= 3840) && (commandValue <= 4095)) {// 1111 dfff ffff //1111 0000 0000 bis 1111 1111 1111
            result = add(fValue, 1);                    //The contents of register ’f’ are incremented If ’d’ is 0 the result is placed in the W register.  
            if (result == 0) {                                    //  If ’d’ is 1 the result is placed back in register ’f’. If the result is not 0 , the next instruction is executed.
                storage.countProgramCounter();                     // If the result is 0, a NOP is executed instead making it a 2TCY instruction
            }
            storeValue(storage, d, f, result);
        }

        //IORWF
        if ((commandValue >= 1024) && (commandValue <= 1279)) { // 100 dfff ffff // 100 0000 0000 bis 100 1111 1111
            result = wValue | fValue;                           // The contents of the W register is OR’ed with the eight bit literal 'k'. The result is placed in the W register.
            zeroFlag(storage, result);
            storeValue(storage, d, f, result);
        }

        //MOVF
        if ((commandValue >= 2048) && (commandValue <= 2303)) {// 1000 dfff ffff //1000 0000 0000 bis 1000 1111 1111
            zeroFlag(storage, fValue);                          //The contents of register f is moved to a destination dependant upon the status of d. 
            storeValue(storage, d, f, fValue);                  // If d = 0, destination is W register. If d = 1, the destination is file register f itself. 
                                                                // d = 1 is useful to test a file register since status flag Z is affected
        }

        //MOVWF
        if ((commandValue >= 128) && (commandValue <= 255)) { // 1000 dfff ffff // 1000 0000 bis 1111 1111
            storage.setRegisterMemory(f, wValue);              // Move data from W register to register
        }

        //NOP
        if ((commandValue == 0) || (commandValue == 32) || (commandValue == 64) || (commandValue == 96)) {// 1fff ffff //0    10 0000    100 0000    110 0000                            //No operation
        }

        //RLF
        if ((commandValue >= 3328) && (commandValue <= 3583)) {// 00 0000 0xx0 0000 // 1101 0000 0000 bis 1101 1111 1111
            memory = fValue & 0x0080;                           // The contents of register ’f’ are rotated one bit to the left through the Carry Flag. 
            result = fValue << 1;                               // If ’d’ is 0 the result is placed in the W register. If ’d’ is 1 the result is stored back in register ’f’.
            if (storage.registerMemoryGetC() == 1) {
                result = result | 0x0001;
            }
            if (memory == 0x0080) {
                storage.registerMemorySetC();
            } else {
                storage.registerMemoryResetC();
            }
            result = result & ~0xFF00;
            storeValue(storage, d, f, result);
        }

        //RRF
        if ((commandValue >= 3072) && (commandValue <= 3327)) {// 1100 dfff ffff //1100 0000 0000 bis 1100 1111 1111
            memory = fValue & 0x0001;                               //The contents of register ’f’ are rotated one bit to the right through the Carry Flag. 
            result = fValue >>> 1;                                  //If ’d’ is 0 the result is placed in the W register. If ’d’ is 1 the result is placed back in register ’f’.
            if (storage.registerMemoryGetC() == 1) {
                result = result | 0x0080;
            }
            if (memory == 0x0001) {
                storage.registerMemorySetC();
            } else {
                storage.registerMemoryResetC();
            }
            storeValue(storage, d, f, result);
        }

        //SUBWF
        if ((commandValue >= 512) && (commandValue <= 767)) {// 10 dfff ffff //10 0000 0000 bis 10 1111 1111
            result = subtract(fValue, wValue);               // The contents of W register is subtracted (2’s complement method) from the eight bit literal 'k'. The result is placed in the W register.
            carryFlag(storage);
            digitCarryFlag(storage);
            resetAddSubFlag();
            zeroFlag(storage, result);
            storeValue(storage, d, f, result);
        }

        //SWAPF
        if ((commandValue >= 3584) && (commandValue <= 3839)) {// 1110 dfff ffff //1110 0000 0000 bis 1110 1111 1111
            result = ((fValue & 0xf0) >> 4) | ((fValue & 0x0f) << 4);       // The upper and lower nibbles of contents of register 'f' are exchanged. 
            storeValue(storage, d, f, result);                              // If 'd' is 0 the result is placed in W register. If 'd' is 1 the result is placed in register 'f'.
        }

        //XORWF
        if ((commandValue >= 1536) && (commandValue <= 1791)) {// 1110 dfff ffff //110 0000 0000 bis 110 1111 1111
            result = wValue ^ fValue;                           //The contents of the W register are XOR’ed with the eight bit literal 'k'. The result is placed in the W register.
            zeroFlag(storage, result);
            storeValue(storage, d, f, result);
        }

        //----

        //BCF       Bit b in register f is cleared
        if ((commandValue >= 4096) && (commandValue <= 5119)) {//1 0000 0000 0000 bis 1 0011 1111 1111
            switch (b) {
                case 0:
                    result = fValue & ~0x0001;
                    break;
                case 128:
                    result = fValue & ~0x0002;
                    break;
                case 256:
                    result = fValue & ~0x0004;
                    break;
                case 384:
                    result = fValue & ~0x0008;
                    break;
                case 512:
                    result = fValue & ~0x0010;
                    break;
                case 640:
                    result = fValue & ~0x0020;
                    break;
                case 768:
                    result = fValue & ~0x0040;
                    break;
                case 896:
                    result = fValue & ~0x0080;
                    break;
                default:
						break;
            }
            storage.setRegisterMemory(f, result);
        }

        //BSF       Bit b in register f is set
        if ((commandValue >= 5120) && (commandValue <= 6143)) {//1 0100 0000 0000 bis 1 0111 1111 1111
            switch (b) {
                case 0:
                    result = fValue | 0x0001;
                    break;
                case 128:
                    result = fValue | 0x0002;
                    break;
                case 256:
                    result = fValue | 0x0004;
                    break;
                case 384:
                    result = fValue | 0x0008;
                    break;
                case 512:
                    result = fValue | 0x0010;
                    break;
                case 640:
                    result = fValue | 0x0020;
                    break;
                case 768:
                    result = fValue | 0x0040;
                    break;
                case 896:
                    result = fValue | 0x0080;
                    break;
                default:
					break;
            }
            storage.setRegisterMemory(f, result);
        }

        //BTFSC     Bit Test f, Skip if Clear           if bit b in register f is 1 -> next instruction is executed; if bit b in register f is 0 -> next instruction is discarded and NOP is excecuted
        if ((commandValue >= 6144) && (commandValue <= 7167)) {//1 1000 0000 0000 bis 1 1011 1111 1111
            switch (b) {
                case 0:
                    memory = fValue & 0x0001;
                    break;
                case 128:
                    memory = fValue & 0x0002;
                    break;
                case 256:
                    memory = fValue & 0x0004;
                    break;
                case 384:
                    memory = fValue & 0x0008;
                    break;
                case 512:
                    memory = fValue & 0x0010;
                    break;
                case 640:
                    memory = fValue & 0x0020;
                    break;
                case 768:
                    memory = fValue & 0x0040;
                    break;
                case 896:
                    memory = fValue & 0x0080;
                    break;
                default:
                    break;
            }
            if (memory == 0) {
                storage.countProgramCounter();
            }
        }

        //BTFSS         Bit Test f, Skip if Set         if bit b in register f is 0 then the next instruction is executed; if bit b is 1 -> next instruction is discarded and a NOP is executed
        if ((commandValue >= 7168) && (commandValue <= 8191)) {//1 1100 0000 0000 bis 1 1111 1111 1111
            switch (b) {
                case 0:
                    memory = fValue & 0x0001;
                    break;
                case 128:
                    memory = fValue & 0x0002;
                    break;
                case 256:
                    memory = fValue & 0x0004;
                    break;
                case 384:
                    memory = fValue & 0x0008;
                    break;
                case 512:
                    memory = fValue & 0x0010;
                    break;
                case 640:
                    memory = fValue & 0x0020;
                    break;
                case 768:
                    memory = fValue & 0x0040;
                    break;
                case 896:
                    memory = fValue & 0x0080;
                    break;
                default:
						break;
            }
            if (memory != 0) {
                storage.countProgramCounter();
            }
        }

        //----

        //ADDLW     add k and w
        if ((commandValue >= 15872) && (commandValue <= 16383)) {  //von 11 1110 0000 0000 bis 11 1111 1111 1111
            result = add(wValue, k);                                
            carryFlag(storage);
            digitCarryFlag(storage);
            resetAddSubFlag();
            zeroFlag(storage, result);
            storage.setWRegister(result);
        }

        //ANDLW     AND k with W
        if ((commandValue >= 14592) && (commandValue <= 14847)) {   // 11 1001 0000 0000 bis 11 1001 1111 1111
            result = wValue & k;
            zeroFlag(storage, result);
            storage.setWRegister(result);
        }

        //CALL      Call subroutine         return address pushed onto stack, 11 bit immediate address loaded into PC bits, the upper bits of the pc are loaded from pclath
        if ((commandValue >= 8192) && (commandValue <= 10239)) { // 10 0000 0000 0000 bis 10 0111 1111 1111
            storage.pushEightLevelStack(storage.getProgramCounter());
            storage.programCounterSetPCLATHPart(kSpecial);
        }

        //CLRWDT clears watchdog timer
        if (commandValue == 100) {  // 00 0000 0110 0100
            storage.resetWatchDogTimer();
            storage.resetPrescaler();
            storage.registerMemorySetPD();
            storage.registerMemorySetTO();
        }

        //GOTO      go to address
        if ((commandValue >= 10240) && (commandValue <= 12287)) {//10 1000 0000 0000 bis 10 1111 1111 1111
            storage.programCounterSetPCLATHPart(kSpecial);
        }

        //IORLW     Inclusive OR k with W
        if ((commandValue >= 14336) && (commandValue <= 14591)) { // 11 1000 0000 0000 bis 11 1000 1111 1111
            result = wValue | k;
            zeroFlag(storage, result);
            storage.setWRegister(result);
        }

        //MOVLW     move k into w register
        if ((commandValue >= 12288) && (commandValue <= 13311)) { //11 0000 0000 0000 bis 11 0011 1111 1111
            storage.setWRegister(k);
        }

        //RETFIE    Return from interrupt       stack poped and top of stack loaded into PC, interrupts are enabled by setting global interrupt enable bit
        if (commandValue == 9) { //1001
            storage.setProgramCounter(storage.popEightLevelStack());
            storage.registerMemorySetGIE();
        }

        //RETLW         Return with k in W 
        if ((commandValue >= 13312) && (commandValue <= 14335)) { //11 0100 0000 0000 bis 11 0111 1111 1111
            storage.setProgramCounter(storage.popEightLevelStack());
            storage.setWRegister(k);
        }

        //RETURN        Return from Subroutine  popped stack and load into program counter
        if (commandValue == 8) { //1000
            storage.setProgramCounter(storage.popEightLevelStack());
        }

        //SLEEP     Go into standby mode
        if (commandValue == 99) {//110 0011
            storage.registerMemoryResetPD();
            storage.registerMemorySetTO();
            storage.resetWatchDogTimer();
            storage.resetPrescaler();
        }

        //SUBLW     Subtract W from k
        if ((commandValue >= 15360) && (commandValue <= 15871)) {//11 1100 0000 0000 bis 11 1101 1111 1111
            result = subtract(k, wValue);
            carryFlag(storage);
            digitCarryFlag(storage);
            resetAddSubFlag();
            zeroFlag(storage, result);
            storage.setWRegister(result);
        }

        //XORLW         Exclusive OR k with W and save in wregister
        if ((commandValue >= 14848) && (commandValue <= 15103)) {//11 1010 0000 0000 bis 11 1010 1111 1111
            result = wValue ^ k;
            zeroFlag(storage, result);
            storage.setWRegister(result);
        }

        storage.registerMemoryCountTMR0(pic);
        storage.setPrescaler();
        storage.countWatchDogTimer();
        storage.countRunTime();
        storage.registerMemoryCheckInterrupt();
    }
// sets carryflag
    public void carryFlag(Storage storage) {
        if ((CAdd == 1) || (CSubtract == 1)) {
            storage.registerMemorySetC();
        } else {
            storage.registerMemoryResetC();
        }
    }

// sets digitCarryFlag dc
    public void digitCarryFlag(Storage storage) {
        if ((DCAdd == 1) || (DCSubtract == 1)) {
            storage.registerMemorySetDC();
        } else {
            storage.registerMemoryResetDC();
        }
    }
// sets zeroflag z
    public void zeroFlag(Storage storage, int value) {
        if (value == 0) {
            storage.registerMemorySetZ();
        } else {
            storage.registerMemoryResetZ();
        }
    }

    // resets the flags for add
    public void resetAddSubFlag() {
        CAdd = 0;
        CSubtract = 0;
        DCAdd = 0;
        DCSubtract = 0;
    }

// add function 
    public int add(int valueOne, int valueTwo) {
        int result = valueOne + valueTwo;
        if (((valueOne & 0x0008) == 8) && ((valueTwo & 0x0008) == 8)) {         
            DCAdd = 1;                                                      // sets digit carry
        }
        if (result > 255) {
            CAdd = 1;                                                       // sets carry bit
            result = result - 256;
        }
        return result;
    }

// subtract function
    public int subtract(int valueOne, int valueTwo) {
        int result = valueOne + ~valueTwo + 1;
        if (result >= 0) {
            CSubtract = 1;                                                  // sets carry bit
            DCSubtract = 1;                                                 // sets digit carry bit
        }
        if (result < 0) {
            result = 256 + result;                              
        }
        return result;
    }

    public void storeValue(Storage storage, int d, int f, int value) {
        if (d > 0) {
            storage.setRegisterMemory(f, value);
            if (f == 2) {
                storage.programCounterSetPCLATH(value);
            }
        } else {
            storage.setWRegister(value);
        }
    }
}
